package team046_v02;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;

public class Util {
	private MapLocation hq;
	public static Direction[] directions = {Direction.NORTH, Direction.NORTH_EAST, Direction.EAST, Direction.SOUTH_EAST, Direction.SOUTH, Direction.SOUTH_WEST, Direction.WEST, Direction.NORTH_WEST};
	
	public Util(MapLocation hq) {
		this.hq = hq;
	}
	
	public int convertFromMapLocation(MiningLocation location) {
		location.x = location.x - hq.x;
		location.y = location.y - hq.y;
		int mask = 0x03FF;
		return ((((location.x & mask) << 10) | (location.y & mask)) << 10) | (location.amount & mask);
	}

	public MiningLocation convertToMiningLocation(int read) {
		
		int mask = 0x03FF;
		
		if(read == 0) {
			return null;
		} else {
			int amount = (read & mask);
			String vs1 = Integer.toBinaryString(((read >>> 10) & mask));
			if(vs1.startsWith("1") && vs1.length() == 10) { vs1 = "111111" + vs1; }
			int y = Integer.valueOf(vs1, 2).shortValue();
			vs1 = Integer.toBinaryString(((read >>> 20) & mask));
			if(vs1.startsWith("1") && vs1.length() == 10) { vs1 = "111111" + vs1; }
			int x = Integer.valueOf(vs1, 2).shortValue();
			return new MiningLocation(x + hq.x, y + hq.y, amount);
		}
	}
	
	static int directionToInt(Direction d) {
		switch(d) {
			case NORTH:
				return 0;
			case NORTH_EAST:
				return 1;
			case EAST:
				return 2;
			case SOUTH_EAST:
				return 3;
			case SOUTH:
				return 4;
			case SOUTH_WEST:
				return 5;
			case WEST:
				return 6;
			case NORTH_WEST:
				return 7;
			default:
				return -1;
		}
	}
	
	public enum Channel {
		
		AEROSPACELAB_COUNT(0),
		BARRACKS_COUNT(1),
		BASHER_COUNT(2),
		BEAVER_COUNT(3),
		COMMANDER_COUNT(4),
		COMPUTER_COUNT(5),
		DRONE_COUNT(6),
		HANDWASHSTATION_COUNT(7),
		HELIPAD_COUNT(8),
		LAUNCHER_COUNT(9),
		MINER_COUNT(10),
		MINERFACTORY_COUNT(11),
		MISSILE_COUNT(12),
		SOLDIER_COUNT(13),
		SUPPLYDEPOT_COUNT(14),
		TANK_COUNT(15),
		TANKFACTORY_COUNT(16),
		TECHNOLOGYINSTITUTE_COUNT(17),
		TOWER_COUNT(18),
		TRAININGFIELD_COUNT(19),
		MININGTOP5_POSITIONS_OFFSET(100),// Should only be between 100 and 150
		ALLIN_OFFSET(200), 
		;
		
		private int value;

		private Channel(int value) {
			this.value = value;
		}
		
		public int getValue() {
			return value;
		}
		
	}
	
	public static void broadcast(RobotController rc, int index, int data) {
		try {
			rc.broadcast(index, data);
		} catch (GameActionException e) {
			e.printStackTrace();
		}
	}
}
