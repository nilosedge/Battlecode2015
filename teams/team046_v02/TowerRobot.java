package team046_v02;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;

public class TowerRobot extends BaseRobot {

	public TowerRobot(RobotController rc, Strategy strat) {
		super(rc, strat);

	}

	@Override
	public void run() throws GameActionException {

		if (rc.isWeaponReady()) {
			attackSomething();
		}
		strat.sendSuppliesFromBuilding();
	}

}
