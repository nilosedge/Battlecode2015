package team046_v02;

import battlecode.common.RobotController;


public class RobotPlayer {
	
	public static void run(RobotController rc) {
		
		BaseRobot robot = null;
		
		try {
			Strategy strat = StrategyManager.createStrategy(rc);
			switch(rc.getType()) {
				case HQ:
					robot = new HQRobot(rc, strat);
					break;
				case AEROSPACELAB:
					robot = new SpawnBuildingRobot(rc, strat);
					break;
				case BARRACKS:
					robot = new SpawnBuildingRobot(rc, strat);
					break;
				case BASHER:
					robot = new BasherRobot(rc, strat);
					break;
				case BEAVER:
					robot = new BeaverRobot(rc, strat);
					break;	
				case COMMANDER:
					robot = new CommanderRobot(rc, strat);
					break;
				case COMPUTER:
					robot = new ComputerRobot(rc, strat);
					break;
				case DRONE:
					robot = new DroneRobot(rc, strat);
					break;
				case HANDWASHSTATION:
					robot = new HandwashingStationRobot(rc, strat);
					break;
				case HELIPAD:
					robot = new SpawnBuildingRobot(rc, strat);
					break;
				case LAUNCHER:
					robot = new LauncherRobot(rc, strat);
					break;
				case MINER:
					robot = new MinerRobot(rc, strat);
					break;
				case MINERFACTORY:
					robot = new SpawnBuildingRobot(rc, strat);
					break;
				case MISSILE:
					robot = new MissleRobot(rc, strat);
					break;
				case SOLDIER:
					robot = new SoldierRobot(rc, strat);
					break;
				case SUPPLYDEPOT:
					robot = new SupplyDepotRobot(rc, strat);
					break;
				case TANK:
					robot = new TankRobot(rc, strat);
					break;
				case TANKFACTORY:
					robot = new SpawnBuildingRobot(rc, strat);
					break;
				case TECHNOLOGYINSTITUTE:
					robot = new SpawnBuildingRobot(rc, strat);
					break;
				case TOWER:
					robot = new TowerRobot(rc, strat);
					break;
				case TRAININGFIELD:
					robot = new SpawnBuildingRobot(rc, strat);
					break;
				default:
					break;
			}
			robot.loop();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
