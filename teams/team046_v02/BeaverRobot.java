package team046_v02;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.RobotController;
import battlecode.common.RobotType;

public class BeaverRobot extends BaseRobot {

	public BeaverRobot(RobotController rc, Strategy strat) {
		super(rc, strat);
	}

	@Override
	public void run() throws GameActionException {

		if (rc.isWeaponReady()) {
			attackSomething();
		}
		if (rc.isCoreReady()) {
			strat.updateCounts();
			
			RobotJob job = strat.getNextAssignment();
			
			rc.setIndicatorString(0, "Job: " + job);
			
			if(job.getJobtype() == RobotJob.RobotJobType.BUILD) {
				if(rc.getTeamOre() > job.getRobottype().oreCost) {
					rc.setIndicatorString(0, "Building: " + job.getRobottype());
					tryBuild(Util.directions[rand.nextInt(8)],job.getRobottype());
				} else {
					rc.mine();
				}
			} else if(job.getJobtype() == RobotJob.RobotJobType.MINE) {
				rc.setIndicatorString(0, "Mineing: " + job.getRobottype());
				// TODO Check current ore value move to better spot if low
				// else mine
				rc.mine();
			} else if(job.getJobtype() == RobotJob.RobotJobType.MOVE) {
				if(job.getLocation() != null) {
					rc.setIndicatorString(0, "Moving: " + job.getLocation());
					tryMove(strat.myLocation.directionTo(job.getLocation()));
				} else {
					if(strat.myLocation.distanceSquaredTo(strat.hqLocation) > 40) {
						rc.setIndicatorString(0, "Moving: " + strat.hqLocation);
						tryMove(strat.myLocation.directionTo(strat.hqLocation));
					} else {
						rc.setIndicatorString(0, "Moving: Random");
						tryMove(Util.directions[rand.nextInt(8)]);
					}
				}
			} else {

				if(strat.myLocation.distanceSquaredTo(strat.hqLocation) > 40) {
					rc.setIndicatorString(0, "Moving: " + strat.hqLocation);
					tryMove(strat.myLocation.directionTo(strat.hqLocation));
				} else {
					rc.setIndicatorString(0, "Moving: Random");
					tryMove(Util.directions[rand.nextInt(8)]);
				}
			}
		}

	}

	// This method will attempt to build in the given direction (or as close to it as possible)
	public void tryBuild(Direction d, RobotType type) throws GameActionException {
		int offsetIndex = 0;
		int[] offsets = {0,1,-1,2,-2,3,-3,4};
		int dirint = Util.directionToInt(d);
		while (offsetIndex < 8 && !rc.canMove(Util.directions[(dirint+offsets[offsetIndex]+8)%8])) {
			offsetIndex++;
		}
		if (offsetIndex < 8) {
			rc.build(Util.directions[(dirint+offsets[offsetIndex]+8)%8], type);
		}
	}
}
