package team046_v02;

import java.util.ArrayList;

import battlecode.common.RobotController;
import battlecode.common.RobotType;

public class StrategyManager {

	public static Strategy createStrategy(RobotController rc) {
		
		Strategy strat = null;
		if(rc.getType() == RobotType.AEROSPACELAB) {
			ArrayList<RobotTypeInfo> list = new ArrayList<RobotTypeInfo>();
			list.add(new RobotTypeInfo(RobotType.LAUNCHER, 100, Util.Channel.LAUNCHER_COUNT.getValue(), 40));
			strat = new StrategySpawnBuilding(rc, list);
		} else if(rc.getType() == RobotType.BARRACKS) {
			ArrayList<RobotTypeInfo> list = new ArrayList<RobotTypeInfo>();
			list.add(new RobotTypeInfo(RobotType.BASHER, 60, Util.Channel.BASHER_COUNT.getValue(), 15));
			list.add(new RobotTypeInfo(RobotType.SOLDIER, 40, Util.Channel.SOLDIER_COUNT.getValue(), 15));
			strat = new StrategySpawnBuilding(rc, list);
		} else if(rc.getType() == RobotType.BASHER) {
			strat = new StrategyBasher(rc);
		} else if(rc.getType() == RobotType.BEAVER) {
			//strat = new BeaverBuildingStrategySmallMap(rc);
			//strat = new BeaverBuildingStrategyMediumMap(rc);
			strat = new StrategyBeaverLargeMap(rc);
		} else if(rc.getType() == RobotType.COMMANDER) {
		} else if(rc.getType() == RobotType.COMPUTER) {
		} else if(rc.getType() == RobotType.DRONE) {
			strat = new StrategyDrone(rc);
		} else if(rc.getType() == RobotType.HANDWASHSTATION) {
			strat = new StrategyDoNothing(rc);
		} else if(rc.getType() == RobotType.HELIPAD) {
			// Large Map 100
			// Medium 40
			// Small 20
			ArrayList<RobotTypeInfo> list = new ArrayList<RobotTypeInfo>();
			list.add(new RobotTypeInfo(RobotType.DRONE, 100, Util.Channel.DRONE_COUNT.getValue(), 100));
			strat = new StrategySpawnBuilding(rc, list);
		} else if(rc.getType() == RobotType.HQ) {
			//strat = new HQStrategyMediumMap(rc);
			//strat = new HQStrategySmallMap(rc);
			strat = new StrategyHQ(rc, 2);
		} else if(rc.getType() == RobotType.LAUNCHER) {
		} else if(rc.getType() == RobotType.MINER) {
			//strat = new MinerStrategySmallMap(rc);
			//strat = new MinerStrategyMediumMap(rc);
			// Large mineLowAmount = 12;
			// Medium mineLowAmount = 8;
			// Small mineLowAmount = 4;
			strat = new StrategyMiner(rc, 8);
		} else if(rc.getType() == RobotType.MINERFACTORY) {
			// Large 40
			// Medium 20
			// Small 5
			ArrayList<RobotTypeInfo> list = new ArrayList<RobotTypeInfo>();
			list.add(new RobotTypeInfo(RobotType.MINER, 100, Util.Channel.MINER_COUNT.getValue(), 40));
			strat = new StrategySpawnBuilding(rc, list);
			
		} else if(rc.getType() == RobotType.MISSILE) {
		} else if(rc.getType() == RobotType.SOLDIER) {
			strat = new StrategySoldier(rc);
		} else if(rc.getType() == RobotType.SUPPLYDEPOT) {
			strat = new StrategyDoNothing(rc);
		} else if(rc.getType() == RobotType.TANK) {
			strat = new StrategyTank(rc);
		} else if(rc.getType() == RobotType.TANKFACTORY) {
			// Large 80?
			// Medium 20
			// Small 5
			ArrayList<RobotTypeInfo> list = new ArrayList<RobotTypeInfo>();
			list.add(new RobotTypeInfo(RobotType.TANK, 100, Util.Channel.TANK_COUNT.getValue(), 80));
			strat = new StrategySpawnBuilding(rc, list);
		} else if(rc.getType() == RobotType.TECHNOLOGYINSTITUTE) {
			ArrayList<RobotTypeInfo> list = new ArrayList<RobotTypeInfo>();
			list.add(new RobotTypeInfo(RobotType.COMPUTER, 100, Util.Channel.COMPUTER_COUNT.getValue(), 4));
			strat = new StrategySpawnBuilding(rc, list);
		} else if(rc.getType() == RobotType.TOWER) {
			strat = new StrategyDoNothing(rc);
		} else if(rc.getType() == RobotType.TRAININGFIELD) {
			ArrayList<RobotTypeInfo> list = new ArrayList<RobotTypeInfo>();
			list.add(new RobotTypeInfo(RobotType.COMMANDER, 100, Util.Channel.COMMANDER_COUNT.getValue(), 1));
			strat = new StrategySpawnBuilding(rc, list);
		}

		return strat;
		
	}
}
