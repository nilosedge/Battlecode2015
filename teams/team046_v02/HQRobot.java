package team046_v02;

import battlecode.common.Clock;
import battlecode.common.GameActionException;
import battlecode.common.RobotController;

public class HQRobot extends BaseRobot {

	public HQRobot(RobotController rc, Strategy strat) throws GameActionException {
		super(rc, strat);
	}

	public void run() throws GameActionException {

		if (rc.isWeaponReady()) {
			attackSomething();
		}

		if(Clock.getRoundNum() % 15 == 0) {
			((StrategyHQ)strat).setMiningPoints();
		}
		
		strat.updateCounts();
		
		//RobotJob[] jobs = strat.getGlobalJobs();
		
		if (rc.isCoreReady()) {
			RobotJob job = strat.getNextAssignment();
			if(job != null && job.getJobtype() == RobotJob.RobotJobType.SPAWN && rc.getTeamOre() > job.getRobottype().oreCost) {
				trySpawn(Util.directions[rand.nextInt(8)], job.getRobottype());
			}
			strat.sendSuppliesFromBuilding();
			rc.setIndicatorString(0, "Total Ore: " + rc.getTeamOre());
		}
		
		// Large Map 1600
		// Medium Map 1700
		// Small Map ??
		
		if(Clock.getRoundNum() >= 1600) {
			rc.setIndicatorString(1, "Sending ALLIN Message");
			Util.broadcast(rc, Util.Channel.ALLIN_OFFSET.getValue(), 1);
		}
	}




}