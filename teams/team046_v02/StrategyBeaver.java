package team046_v02;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;

public abstract class StrategyBeaver extends Strategy {

	protected int SUPPLYDEPOT_COUNT = 0;
	protected int TECHNOLOGYINSTITUTE_COUNT = 0;
	protected int BARRACKS_COUNT = 0;
	protected int HELIPAD_COUNT = 0;
	protected int HANDWASHSTATION_COUNT = 0;
	protected int MINERFACTORY_COUNT = 0;
	protected int TRAININGFIELD_COUNT = 0;
	protected int TANKFACTORY_COUNT = 0;
	protected int AEROSPACELAB_COUNT = 0;
	
	protected int MINER_COUNT = 0;
	
	public StrategyBeaver(RobotController rc) {
		super(rc);
		updateCounts();
	}
	
	public void updateCounts() {
		try { SUPPLYDEPOT_COUNT = rc.readBroadcast(Util.Channel.SUPPLYDEPOT_COUNT.getValue()); } catch (GameActionException e) {}
		try { TECHNOLOGYINSTITUTE_COUNT = rc.readBroadcast(Util.Channel.TECHNOLOGYINSTITUTE_COUNT.getValue()); } catch (GameActionException e) {}
		try { BARRACKS_COUNT = rc.readBroadcast(Util.Channel.BARRACKS_COUNT.getValue()); } catch (GameActionException e) {}
		try { HELIPAD_COUNT = rc.readBroadcast(Util.Channel.HELIPAD_COUNT.getValue()); } catch (GameActionException e) {}
		try { HANDWASHSTATION_COUNT = rc.readBroadcast(Util.Channel.HANDWASHSTATION_COUNT.getValue()); } catch (GameActionException e) {}
		try { MINERFACTORY_COUNT = rc.readBroadcast(Util.Channel.MINERFACTORY_COUNT.getValue()); } catch (GameActionException e) {}
		try { TRAININGFIELD_COUNT = rc.readBroadcast(Util.Channel.TRAININGFIELD_COUNT.getValue()); } catch (GameActionException e) {}
		try { TANKFACTORY_COUNT = rc.readBroadcast(Util.Channel.TANKFACTORY_COUNT.getValue()); } catch (GameActionException e) {}
		try { AEROSPACELAB_COUNT = rc.readBroadcast(Util.Channel.AEROSPACELAB_COUNT.getValue()); } catch (GameActionException e) {}
		try { MINER_COUNT = rc.readBroadcast(Util.Channel.MINER_COUNT.getValue()); } catch (GameActionException e) {}
		myLocation = rc.getLocation();
	}

	public abstract RobotJob getNextAssignment();
}
