package team046_v02;

import java.util.ArrayList;
import java.util.HashMap;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;
import battlecode.common.RobotType;

public class StrategySpawnBuilding extends Strategy {

	private HashMap<RobotType, Integer> counts = new HashMap<RobotType, Integer>();
	private ArrayList<RobotTypeInfo> infos;
	
	public StrategySpawnBuilding(RobotController rc, ArrayList<RobotTypeInfo> infos) {
		super(rc);
		this.infos = infos;
		updateCounts();
	}

	@Override
	public void updateCounts() {
		for(RobotTypeInfo info: infos) {
			try {
				counts.put(info.type, rc.readBroadcast(info.count_channel));
			} catch (GameActionException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public RobotJob getNextAssignment() {
		
		int fate = rand.nextInt(10000);
		
		for(RobotTypeInfo info: infos) {
			if(counts.get(info.type).intValue() < info.max && fate <= (info.percentage * 100) || rc.getTeamOre() > 1050) {
				RobotJob job = new RobotJob(RobotJob.RobotJobType.SPAWN);
				job.setRobottype(info.type);
				return job;
			}
		}
		return null;
	}

	@Override
	public void completeJob(RobotJob job) {
		// TODO Auto-generated method stub
		
	}

}
