package lemings;

import battlecode.common.RobotController;


public class RobotPlayer {
	
	public static void run(RobotController rc) {

		// BC 35
		RobotSetup rs = new RobotSetup();
		rs.rc = rc;
		BroadcastSytem bc = new BroadcastSytem(rc);
		rs.bc = bc;
		
		try {
			// BC 280
			rs.robot = RobotManager.createRobot(rs);
			MicroSystem.setup = rs;
			NavSystem.init(rs);
			rs.robot.loop();
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
