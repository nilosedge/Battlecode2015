package lemings;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;

public class TankRobot extends StrategyRobot {

	public TankRobot(RobotSetup rs) {
		super(rs);
	}

	@Override
	public void run() throws GameActionException {

		myLocation = rc.getLocation();
		
		MicroSystem.init();
		boolean action = MicroSystem.genericMicro();

		
		if(!action) {
			job = getNextAssignment();
			NavSystem.setDestination(job.getLocation());
			rc.setIndicatorString(0, "Job: " + job);

			if(rc.isCoreReady()) {
				if(job.getJobtype() == RobotJob.RobotJobType.MOVE) {
					NavSystem.tryMoveCloser();
				} else if(job.getJobtype() == RobotJob.RobotJobType.PATROL) {
					if(myLocation.distanceSquaredTo(job.getLocation()) > 15) {
						NavSystem.tryMoveCloser();
					} else {
						NavSystem.tryMoveRandom();
					}
				} else if(job.getJobtype() == RobotJob.RobotJobType.ALLIN) {
					NavSystem.tryMoveCloser();
				} else {
					NavSystem.tryMoveRandom();
				}
			}

		}
		
		sendSupplies();
	}


	@Override
	public RobotJob getNextAssignment() {
		
		RobotJob globalJob = getGlobalJobs();
		if(globalJob != null) {
			return globalJob;
		}
		
		MapLocation[] towers = rc.senseTowerLocations();
		
//		TreeMap<Integer, MapLocation> map = new TreeMap<Integer, MapLocation>();
//		
//		for(MapLocation m: towers) {
//			map.put(myLocation.distanceSquaredTo(m), m);
//		}
//		
//		RobotJob job = new RobotJob(RobotJobType.PATROL);
//		job.setLocation(map.firstEntry().getValue());
//		return job;
		
		MapLocation patrolLocation;
		
		int fate = rand.nextInt(towers.length);
		
		if(towers.length > 1) {
			patrolLocation = towers[fate];
		} else if(towers.length == 1) {
			patrolLocation = towers[0];
		} else {
			patrolLocation = rc.senseEnemyHQLocation();
		}
		
		RobotJob job = new RobotJob(RobotJob.RobotJobType.PATROL);
		job.setLocation(patrolLocation);
		return job;

	}


}
