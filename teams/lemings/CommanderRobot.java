package lemings;

import battlecode.common.GameActionException;


public class CommanderRobot extends StrategyRobot {

	public CommanderRobot(RobotSetup rs) {
		super(rs);
	}

	@Override
	public void run() throws GameActionException {
		//sendSupplies();
		
		myLocation = rc.getLocation();
		bc.broadcast(BroadcastChannel.COMMANDER_LOCATION, new BroadcastMapLocation(myLocation, 0));

		MicroSystem.init();
		boolean action = MicroSystem.genericMicro();
			
		if(!action) {
			if(job == null || job.isCompleted()) {
				job = getNextAssignment();
				NavSystem.setDestination(job.getLocation());
			}
			
			if (rc.isCoreReady()) {
				if(job.getJobtype() == RobotJob.RobotJobType.MOVE) {
					NavSystem.tryMoveCloser();
				} else if(job.getJobtype() == RobotJob.RobotJobType.PATROL) {
					if(myLocation.distanceSquaredTo(job.getLocation()) > 15) {
						NavSystem.tryMoveAvoidAttack(MicroSystem.enemies);
					} else {
						NavSystem.tryMoveRandomAvoidAttack(MicroSystem.enemies);
					}
				} else if(job.getJobtype() == RobotJob.RobotJobType.ALLIN) {
					NavSystem.tryMoveAvoidAttack(MicroSystem.enemies);
				} else {
					NavSystem.tryMoveRandom();
				}

				if((job.getJobtype() == RobotJob.RobotJobType.MOVE || job.getJobtype() == RobotJob.RobotJobType.PATROL) && (job.getLocation() == null || job.getLocation().distanceSquaredTo(myLocation) <= 16)) {
					job.setCompleted(true);
				}
			}
		}
		
	}


	@Override
	public RobotJob getNextAssignment() {
		RobotJob globalJob = getGlobalJobs();
		if(globalJob != null) {
			return globalJob;
		}
		
		RobotJob job = new RobotJob(RobotJob.RobotJobType.PATROL);
		job.setLocation(rc.senseEnemyHQLocation());
		return job;
	}


}
