package lemings;

import java.util.ArrayList;

import battlecode.common.RobotType;

public class RobotManager {
	
	public static BaseRobot createRobot(RobotSetup rs) {
		
		RobotType type = rs.rc.getType();

		if(type.isBuilding) {
		
			if(type == RobotType.AEROSPACELAB) {
				return new SpawnBuildingRobot(rs, new RobotTypeInfo(RobotType.LAUNCHER));
			} else if(type == RobotType.BARRACKS) {
				ArrayList<RobotTypeInfo> buildTypes = new ArrayList<RobotTypeInfo>();
				buildTypes.add(new RobotTypeInfo(RobotType.BASHER, null, 60));
				buildTypes.add(new RobotTypeInfo(RobotType.SOLDIER, null, 40));
				return new MultipleSpawnBuildingRobot(rs, buildTypes);
			} else if(type == RobotType.HANDWASHSTATION) {
				return new HandwashingStationRobot(rs);
			} else if(type == RobotType.HELIPAD) {
				// Large Map 100
				// Medium 40
				// Small 20
				return new SpawnBuildingRobot(rs, new RobotTypeInfo(RobotType.DRONE));
			} else if(type == RobotType.HQ) {
				return new HQRobot(rs, new RobotTypeInfo(RobotType.BEAVER));
			} else if(type == RobotType.MINERFACTORY) {
				// Large 40
				// Medium 20
				// Small 5
				return new SpawnBuildingRobot(rs, new RobotTypeInfo(RobotType.MINER));
			} else if(type == RobotType.SUPPLYDEPOT) {
				return new SupplyDepotRobot(rs);
			} else if(type == RobotType.TANKFACTORY) {
				// Large 80?
				// Medium 20
				// Small 5
				return new SpawnBuildingRobot(rs, new RobotTypeInfo(RobotType.TANK));
			} else if(type == RobotType.TECHNOLOGYINSTITUTE) {
				return new SpawnBuildingRobot(rs, new RobotTypeInfo(RobotType.COMPUTER));
			} else if(type == RobotType.TOWER) {
				return new TowerRobot(rs);
			} else if(type == RobotType.TRAININGFIELD) {
				return new SpawnBuildingRobot(rs, new RobotTypeInfo(RobotType.COMMANDER));
			}
		} else {
			if(type == RobotType.BASHER) {
				return new BasherRobot(rs);
			} else if(type == RobotType.BEAVER) {
				return new BeaverRobot(rs);
			} else if(type == RobotType.COMMANDER) {
				return new CommanderRobot(rs);
			} else if(type == RobotType.COMPUTER) {
				return new ComputerRobot(rs);
			} else if(type == RobotType.DRONE) {
				return new DroneRobot(rs);
			} else if(type == RobotType.TANK) {
				return new TankRobot(rs);
			} else if(type == RobotType.LAUNCHER) {
				return new LauncherRobot(rs);
			} else if(type == RobotType.MINER) {
				// Large mineLowAmount = 12;
				// Medium mineLowAmount = 8;
				// Small mineLowAmount = 4;
				return new MinerRobot(rs, 4);
			} else if(type == RobotType.MISSILE) {
			} else if(type == RobotType.SOLDIER) {
				return new SoldierRobot(rs);
			}
		}

		return null;
	}


}
