package lemings;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;

public class DroneRobot extends StrategyRobot {

	public DroneRobot(RobotSetup rs) {
		super(rs);
	}

	@Override
	public void run() throws GameActionException {

		myLocation = rc.getLocation();
		
		MicroSystem.init();
		boolean action = MicroSystem.genericMicro();
		
		if(!action) {

			job = getNextAssignment();
			NavSystem.setDestination(job.getLocation());
			rc.setIndicatorString(1, "Job: " + job);
				
			if(rc.isCoreReady()) {
				if(job.getJobtype() == RobotJob.RobotJobType.MOVE) {
					NavSystem.tryMoveCloser();
				} else if(job.getJobtype() == RobotJob.RobotJobType.MINERHUNTING) {
					NavSystem.tryMoveAvoidAttack(MicroSystem.enemies);
				} else if(job.getJobtype() == RobotJob.RobotJobType.ALLIN) {
					NavSystem.tryMoveCloser();
				} else {
					NavSystem.tryMoveRandom();
				}
			}
	
			if((job.getJobtype() == RobotJob.RobotJobType.MOVE || job.getJobtype() == RobotJob.RobotJobType.PATROL) && (job.getLocation() == null || job.getLocation().distanceSquaredTo(myLocation) <= 16)) {
				job.setCompleted(true);
			}
		}
		
		if(MicroSystem.miners.size() > 0) {
			MapLocation l = MicroSystem.getMinerLocus();
			BroadcastMapLocation bml = bc.readBroadcastMapLocation(BroadcastChannel.MINER_LOCUS);
			if(bml != null) {
				MapLocation nl = new MapLocation((bml.x + l.x) / 2, (bml.y + l.y) / 2);
				bc.broadcast(BroadcastChannel.MINER_LOCUS, new BroadcastMapLocation(nl, MicroSystem.miners.size()));
				NavSystem.setDestination(nl);
			} else {
				bc.broadcast(BroadcastChannel.MINER_LOCUS, new BroadcastMapLocation(l, MicroSystem.miners.size()));
				NavSystem.setDestination(l);
			}
		}
		
		sendSupplies();
		
	}


	@Override
	public RobotJob getNextAssignment() {
		
		RobotJob globalJob = getGlobalJobs();
		if(globalJob != null && globalJob.jobtype == RobotJob.RobotJobType.ALLIN) {
			return globalJob;
		}

		RobotJob job = new RobotJob(RobotJob.RobotJobType.MINERHUNTING);
		BroadcastMapLocation bml = bc.readBroadcastMapLocation(BroadcastChannel.MINER_LOCUS);
		if(bml != null) {
			job.setLocation(bml.getMapLocation());
		} else {
			job.setLocation(enemyLoc);
		}
		return job;
		
//		MapLocation[] towers = rc.senseTowerLocations();
//		MapLocation patrolLocation;
//		
//		int fate = rand.nextInt(towers.length);
//		
//		if(towers.length > 1) {
//			patrolLocation = towers[fate];
//		} else if(towers.length == 1) {
//			patrolLocation = towers[0];
//		} else {
//			patrolLocation = rc.senseEnemyHQLocation();
//		}
		
//		RobotJob job = new RobotJob(RobotJob.RobotJobType.PATROL);
//		job.setLocation(patrolLocation);
//		return job;

	}
}
