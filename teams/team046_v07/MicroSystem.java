package team046_v07;

import java.util.ArrayList;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;

public class MicroSystem {

	public static RobotInfo[] allRobots;
	public static MapLocation myLocation;
	
//	public static TreeMap<Integer, RobotInfo> nonAttackAble = new TreeMap<Integer, RobotInfo>();
//	public static TreeMap<Integer, RobotInfo> attackAble = new TreeMap<Integer, RobotInfo>();
//	public static TreeMap<Integer, RobotInfo> canNotAttackMe = new TreeMap<Integer, RobotInfo>();
//	public static TreeMap<Integer, RobotInfo> canAttackMe = new TreeMap<Integer, RobotInfo>();

//	public static ArrayList<RobotInfo> friends = new ArrayList<RobotInfo>();
	public static ArrayList<RobotInfo> enemies = new ArrayList<RobotInfo>();
	
	public static ArrayList<RobotInfo> miners = new ArrayList<RobotInfo>();
	
	public static int nonAttackAble = 0;
	public static int attackAble = 0;
	public static int canNotAttackMe = 0;
	public static int canAttackMe = 0;
	
	public static int friends = 0;
	//public static int enemies = 0;
	
	public static RobotInfo weakest;
	public static double lowestHealth;
	
	public static RobotSetup setup;
	public static MicroState prevState = MicroState.WAIT;
	public static MapLocation minerLocus;
	
	public static void init() throws GameActionException {
		clear();
		allRobots = setup.rc.senseNearbyRobots(setup.robot.mySensor);
		
		for(int i = allRobots.length; --i >= 0; ) {
			if(allRobots[i].team == setup.robot.myTeam) {
				//friends.add(allRobots[i]);
				friends++;
			} else {
				
				int dist = allRobots[i].location.distanceSquaredTo(myLocation);
				int hisAttackRange = allRobots[i].type.attackRadiusSquared;
				
				if(dist <= setup.robot.myAttack) {
					//attackAble.put(dist, allRobots[i]);
					attackAble++;
					if(allRobots[i].health < lowestHealth) {
						weakest = allRobots[i];
						lowestHealth = allRobots[i].health;
					}
				} 
				if(dist > setup.robot.myAttack) {
					//nonAttackAble.put(dist, allRobots[i]);
					nonAttackAble++;
				} 
				if(dist <= hisAttackRange) {
					//canAttackMe.put(dist, allRobots[i]);
					canAttackMe++;
				} 
				if(dist > hisAttackRange) {
					//canNotAttackMe.put(dist, allRobots[i]);
					canNotAttackMe++;
				}
				//enemies++;
				if(allRobots[i].type == RobotType.MINER) {
					miners.add(allRobots[i]);
				}
				enemies.add(allRobots[i]);
				
			}
		}
		//enemies.add(new RobotInfo(0, setup.robot.enemyTeam, RobotType.HQ, setup.rc.senseEnemyHQLocation(), 0, 0, 0, 0, 0, 0, null, null));
	}
	
	private static void clear() {
		lowestHealth = 10000;
		weakest = null;
//		nonAttackAble = new TreeMap<Integer, RobotInfo>();
//		attackAble = new TreeMap<Integer, RobotInfo>();
//		canAttackMe = new TreeMap<Integer, RobotInfo>();
//		canNotAttackMe = new TreeMap<Integer, RobotInfo>();
//		friends = new ArrayList<RobotInfo>();
		enemies = new ArrayList<RobotInfo>();
		miners = new ArrayList<RobotInfo>();
		nonAttackAble = 0;
		attackAble = 0;
		canAttackMe = 0;
		canNotAttackMe = 0;
		friends = 0;
		//enemies = 0;
	}

	public static boolean outOfRange() {
		return canAttackMe == 0;
		//return canAttackMe.size() == 0;
	}

	public static boolean canKite() {
		//return outOfRange() && attackAble.size() > 0;
		return outOfRange() && attackAble > 0;
	}

	public static MicroState genericMicro() throws GameActionException {

		if((prevState == MicroState.ATTACK || prevState == MicroState.MOVE) && outOfRange()) {
			// do nothing - let attack deply drop -- wait a turn
			setup.rc.setIndicatorString(2, "Wait: F: " + friends + " E: " + enemies.size() + " R: " + outOfRange());
			prevState = MicroState.WAIT;
			return prevState;
		} else if(canKite()) {
			if(weakest != null) {
				setup.rc.setIndicatorString(2, "Kite Attack: F: " + friends + " E: " + enemies.size() + " R: " + outOfRange());
				prevState = MicroState.ATTACK;
				return prevState;
			}
		} else if(enemies.size() > friends) {
		//} else if(enemies > friends && !outOfRange()) {
			if(outOfRange()) {
				setup.rc.setIndicatorString(2, "Move Closer Safe: F: " + friends + " E: " + enemies.size() + " R: " + outOfRange());
				prevState = MicroState.MOVE;
				return MicroState.MOVECLOSERSAFE;
			} else {
				setup.rc.setIndicatorString(2, "Running: F: " + friends + " E: " + enemies.size() + " R: " + outOfRange());
				prevState = MicroState.MOVE;
				return MicroState.RUN;
			}
		//} else {
		} else if(friends >= enemies.size() && enemies.size() > 0) {
			if(friends >= enemies.size() + 4 && enemies.size() > 0 && outOfRange()) {		
			//if(outOfRange()) {
				setup.rc.setIndicatorString(2, "Move Closer Safe: F: " + friends + " E: " + enemies.size() + " R: " + outOfRange());
				prevState = MicroState.MOVE;
				return MicroState.MOVECLOSERSAFETOWERS;
			} else if(friends >= (enemies.size() + 10) && attackAble == 0) { 
				setup.rc.setIndicatorString(2, "Move Closer Safe: F: " + friends + " E: " + enemies.size() + " R: " + outOfRange());
				prevState = MicroState.MOVE;
				return MicroState.MOVECLOSER;
			} else if(attackAble > 0) {
				if(weakest != null) {
					setup.rc.setIndicatorString(2, "Attack: F: " + friends + " E: " + enemies.size() + " R: " + outOfRange());
					prevState = MicroState.ATTACK;
					return prevState;
				}
			} else {
				setup.rc.setIndicatorString(2, "Move Closer: F: " + friends + " E: " + enemies.size() + " R: " + outOfRange());
				prevState = MicroState.MOVE;
				return MicroState.MOVECLOSERSAFETOWERS;
			}
		}
		
		setup.rc.setIndicatorString(2, "doing jobs: F: " + friends + " E: " + enemies.size() + " Prev: " + prevState + " R: " + outOfRange());
		prevState = MicroState.JOB;
		return prevState;
		
	}


	public enum MicroState {
		ATTACK, WAIT, RUN, JOB, MOVE, MOVECLOSER, MOVECLOSERSAFETOWERS, MOVECLOSERSAFE;
	}


	public static MapLocation getMinerLocus() {
		if(miners.size() > 0) {
			int x = 0;
			int y = 0;
			for(RobotInfo m: miners) {
				x += m.location.x;
				y += m.location.y;
			}
			x /= miners.size();
			y /= miners.size();
			return new MapLocation(x, y);
		} else {
			return setup.robot.enemyLoc;
		}
	}
}
