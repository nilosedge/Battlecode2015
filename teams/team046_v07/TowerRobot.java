package team046_v07;

import battlecode.common.GameActionException;

public class TowerRobot extends BaseRobot {

	int lastx;
	int lasty;
	int counter = 0;
	
	public TowerRobot(RobotSetup rs) {
		super(rs);
	}

	@Override
	public void run() throws GameActionException {

		sendSupplies();
		
		BroadcastMapLocation bml = bc.readBroadcastMapLocation(BroadcastChannel.MINER_LOCUS);
		if(bml != null) {
			if(lastx == bml.x && lasty == bml.y) {
				counter++;
			} else {
				lastx = bml.x;
				lasty = bml.y;
				counter = 0;
			}
			if(counter == 10) {
				bc.broadcast(BroadcastChannel.MINER_LOCUS, new BroadcastMapLocation(rc.senseEnemyHQLocation(), 0));
			}
			rc.setIndicatorString(0, bml.toString());
		}
		
		if (rc.isWeaponReady()) {
			attackSomething();
		}
	}

}
