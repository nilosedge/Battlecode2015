package team046_v07;

import battlecode.common.GameActionException;
import battlecode.common.RobotType;


public abstract class StrategyRobot extends BaseRobot {

	public abstract void run() throws GameActionException;

	public StrategyRobot(RobotSetup rs) {
		super(rs);
	}
	
	//public abstract void updateCounts();
	public abstract RobotJob getNextAssignment();
	//public abstract void completeJob(RobotJob job);
	//public abstract ArrayList<RobotTypeInfo> getBuildList();
	
	public RobotJob getGlobalJobs() {

		BroadcastMapLocation commander = bc.readBroadcastMapLocation(BroadcastChannel.COMMANDER_LOCATION);
		

		int allIn = bc.readBroadcast(BroadcastChannel.ALLIN_OFFSET);
		if(allIn != 0) {
			rc.setIndicatorString(2, "Recieved ALLIN moving to HQ");
			RobotJob job = new RobotJob(RobotJob.RobotJobType.ALLIN);
			job.setLocation(rc.senseEnemyHQLocation());
			return job;
		}

		
		if(myType != RobotType.COMMANDER && myType != RobotType.SOLDIER && myType != RobotType.DRONE) {
			if(commander != null) {
				rc.setIndicatorString(2, "Following Commander");
				RobotJob job = new RobotJob(RobotJob.RobotJobType.MOVE);
				job.setLocation(commander.getMapLocation());
				return job;
			}
		}
		
		if(myType != RobotType.SOLDIER && myType != RobotType.DRONE) {
			BroadcastMapLocation rally = bc.readBroadcastMapLocation(BroadcastChannel.RALLY_POINT);
			if(rally != null) {
				rc.setIndicatorString(2, "Recieved RALLY Point");
				RobotJob job = new RobotJob(RobotJob.RobotJobType.MOVE);
				job.setLocation(rally.getMapLocation());
				return job;
			}
		}
		
		return null;
		
		
		// TODO check other battle points and return one of those
		
		// TODO check other HQ jobs
		// TODO Check HQ jobs queue
		
	}
}
