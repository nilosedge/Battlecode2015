package team046_v07;

import battlecode.common.MapLocation;

public class BroadcastMapLocation {
	public int x;
	public int y;
	public int data;
	
	public BroadcastMapLocation(MapLocation location, int data) {
		this.x = location.x;
		this.y = location.y;
		this.data = data;
	}

	public BroadcastMapLocation(int x, int y, int data) {
		this.x = x;
		this.y = y;
		this.data = data;
	}

	public MapLocation getMapLocation() {
		return new MapLocation(x, y);
	}
	
	public String toString() {
		return "X: " + x + " Y: " + y + " A: " + data;
	}
}
