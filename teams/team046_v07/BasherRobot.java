package team046_v07;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;

public class BasherRobot extends StrategyRobot {
	
	public BasherRobot(RobotSetup rs) {
		super(rs);
	}

	@Override
	public void run() throws GameActionException {
		
		sendSuppliesToRobots();
		
		myLocation = rc.getLocation();
		
		if (rc.isCoreReady()) {
			
			if(job == null || job.isCompleted()) {
				job = getNextAssignment();
				rc.setIndicatorString(0, "New Job: " + job);
			}
			
			if(job.getJobtype() == RobotJob.RobotJobType.MOVE) {
				NavSystem.tryMove(myLocation.directionTo(job.getLocation()));
			} else if(job.getJobtype() == RobotJob.RobotJobType.PATROL) {
				if(myLocation.distanceSquaredTo(job.getLocation()) > 15) {
					NavSystem.tryMove(myLocation.directionTo(job.getLocation()));
				} else {
					NavSystem.tryMove(Util.directions[rand.nextInt(8)]);
				}
			} else {
				NavSystem.tryMove(Util.directions[rand.nextInt(8)]);
			}
			if((job.getJobtype() == RobotJob.RobotJobType.MOVE || job.getJobtype() == RobotJob.RobotJobType.PATROL) && (job.getLocation() == null || job.getLocation().distanceSquaredTo(myLocation) <= 16)) {
				job.setCompleted(true);
			}
		}

	}


	public RobotJob getNextAssignment() {
		
		RobotJob globalJob = getGlobalJobs();
		if(globalJob != null) {
			return globalJob;
		}
		
		MapLocation[] towers = rc.senseTowerLocations();
		
//		TreeMap<Integer, MapLocation> map = new TreeMap<Integer, MapLocation>();
//		
//		for(MapLocation m: towers) {
//			map.put(myLocation.distanceSquaredTo(m), m);
//		}
//		
//		RobotJob job = new RobotJob(RobotJobType.PATROL);
//		job.setLocation(map.firstEntry().getValue());
//		return job;
		
		
		int fate = rand.nextInt(towers.length);
		
		RobotJob job = new RobotJob(RobotJob.RobotJobType.PATROL);
		job.setLocation(towers[fate]);
		return job;

	}
	
}
