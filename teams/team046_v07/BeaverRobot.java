package team046_v07;

import battlecode.common.Clock;
import battlecode.common.GameActionException;
import battlecode.common.RobotType;

public class BeaverRobot extends StrategyRobot {

	protected CountInfo AEROSPACELAB;
	protected CountInfo BARRACKS;
	protected CountInfo HANDWASHSTATION;
	protected CountInfo HELIPAD;
	protected CountInfo MINERFACTORY;
	protected CountInfo SUPPLYDEPOT;
	protected CountInfo TANKFACTORY;
	protected CountInfo TECHNOLOGYINSTITUTE;
	protected CountInfo TRAININGFIELD;
	protected CountInfo TOWER;
	//protected HashMap<RobotType, RobotTypeInfo> counts = new HashMap<RobotType, RobotTypeInfo>();
	
	public BeaverRobot(RobotSetup rs) {
		super(rs);
	}

	@Override
	public void run() throws GameActionException {
		
		if (rc.isWeaponReady()) {
			attackSomething();
		}
		
		myLocation = rc.getLocation();
		teamOre = rc.getTeamOre();
		
		updateCounts();
		
		if (rc.isCoreReady()) {

			if(rc.senseNearbyRobots(9, myTeam).length >= 4) {
				job = new RobotJob(RobotJob.RobotJobType.MOVE);
			} else {
				job = getNextAssignment();
				if(job == null) {
					job = new RobotJob(RobotJob.RobotJobType.MOVE);
				}
			}

			rc.setIndicatorString(0, "Job: " + job);
			
			if(job.getJobtype() == RobotJob.RobotJobType.BUILD) {
				
				if(rc.getTeamOre() >= job.getRobottype().oreCost) {
					rc.setIndicatorString(1, "Building: " + job.getRobottype());
					bc.updateBuildSequence();
					tryBuild(Util.directions[rand.nextInt(8)],job.getRobottype());
				} else {
					if(Clock.getRoundNum() < 18) { 
						rc.setIndicatorString(1, "Not Enough Ore Waiting Mining: " + job.getRobottype());
						rc.mine();
					} else {
						rc.setIndicatorString(1, "Not Enough Ore Waiting: " + job.getRobottype());
					}
				}
			} else if(job.getJobtype() == RobotJob.RobotJobType.MINE) {
				rc.setIndicatorString(1, "Mining: " + job.getRobottype());
				// TODO Check current ore value move to better spot if low
				// else mine
				rc.mine();
			} else if(job.getJobtype() == RobotJob.RobotJobType.MOVE) {
				if(job.getLocation() != null) {
					rc.setIndicatorString(1, "Moving: " + job.getLocation());
					NavSystem.tryMove(myLocation.directionTo(job.getLocation()));
				} else {
					if(myLocation.distanceSquaredTo(hqLocation) > 100) {
						rc.setIndicatorString(1, "Moving: " + hqLocation);
						NavSystem.tryMove(myLocation.directionTo(hqLocation));
					} else {
						rc.setIndicatorString(1, "Moving: Random");
						NavSystem.tryMove(Util.directions[rand.nextInt(8)]);
					}
				}
			} else {

				if(myLocation.distanceSquaredTo(hqLocation) > 100) {
					rc.setIndicatorString(1, "Moving: " + hqLocation);
					NavSystem.tryMove(myLocation.directionTo(hqLocation));
				} else {
					rc.setIndicatorString(1, "Moving: Random");
					NavSystem.tryMove(Util.directions[rand.nextInt(8)]);
				}
			}
		
		} else {
			sendSupplies();
			rc.setIndicatorString(0, "Core Not Ready: " + rc.getCoreDelay());
		}
		rc.setIndicatorString(2, "Ore: " + rc.getTeamOre());
	}

	private void updateCounts() {
		AEROSPACELAB = bc.readBroadcastCount(RobotType.AEROSPACELAB);
		BARRACKS = bc.readBroadcastCount(RobotType.BARRACKS);
		HANDWASHSTATION = bc.readBroadcastCount(RobotType.HANDWASHSTATION);
		HELIPAD = bc.readBroadcastCount(RobotType.HELIPAD);
		MINERFACTORY = bc.readBroadcastCount(RobotType.MINERFACTORY);
		SUPPLYDEPOT = bc.readBroadcastCount(RobotType.SUPPLYDEPOT);
		TANKFACTORY = bc.readBroadcastCount(RobotType.TANKFACTORY);
		TECHNOLOGYINSTITUTE = bc.readBroadcastCount(RobotType.TECHNOLOGYINSTITUTE);
		TRAININGFIELD = bc.readBroadcastCount(RobotType.TRAININGFIELD);
		TOWER = bc.readBroadcastCount(RobotType.TOWER);
	}

	public RobotJob getNextAssignment() {
		int count = 0;
		while(count < 10) {
			RobotType type = bc.getNextBuildSequence();
			if(type != null) {
				CountInfo ci = getCounts(type);
				if(ci.currentCount < ci.maxCount) {
					RobotJob job = new RobotJob(RobotJob.RobotJobType.BUILD);
					job.setRobottype(type);
					return job;
				} else {
					bc.updateBuildSequence();
				}
			}
			count++;
		}

		return null;
	}

	private CountInfo getCounts(RobotType type) {
		//System.out.println("Looking up: " + type);
		if(type == RobotType.AEROSPACELAB) { return AEROSPACELAB; }
		else if(type == RobotType.BARRACKS) { return BARRACKS; }
		else if(type == RobotType.HANDWASHSTATION) { return HANDWASHSTATION; }
		else if(type == RobotType.HELIPAD) { return HELIPAD; }
		else if(type == RobotType.MINERFACTORY) { return MINERFACTORY; }
		else if(type == RobotType.SUPPLYDEPOT) { return SUPPLYDEPOT; }
		else if(type == RobotType.TANKFACTORY) { return TANKFACTORY; }
		else if(type == RobotType.TECHNOLOGYINSTITUTE) { return TECHNOLOGYINSTITUTE; }
		else if(type == RobotType.TRAININGFIELD) { return TRAININGFIELD; }
		else if(type == RobotType.TOWER) { return TOWER; }
		return null;
	}

}
