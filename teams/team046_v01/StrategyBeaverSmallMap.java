package team046_v01;

import battlecode.common.RobotController;
import battlecode.common.RobotType;

public class StrategyBeaverSmallMap extends StrategyBeaver {

	public StrategyBeaverSmallMap(RobotController rc) {
		super(rc);
	}

	@Override
	public RobotJob getNextAssignment() {

		RobotJob globalJob = getGlobalJobs();
		if(globalJob != null) {
			return globalJob;
		}
		
		if(MINERFACTORY_COUNT < 1) {
			RobotJob job = new RobotJob(RobotJob.RobotJobType.BUILD);
			job.setRobottype(RobotType.MINERFACTORY);
			return job;
		}
		
		if(BARRACKS_COUNT < 1) {
			RobotJob job = new RobotJob(RobotJob.RobotJobType.BUILD);
			job.setRobottype(RobotType.BARRACKS);
			return job;
		}
		
		if(TANKFACTORY_COUNT < 1 && MINER_COUNT >= 3) {
			RobotJob job = new RobotJob(RobotJob.RobotJobType.BUILD);
			job.setRobottype(RobotType.TANKFACTORY);
			return job;
		}
		
		RobotJob job = new RobotJob(RobotJob.RobotJobType.MINE);
		return job;
	}

	@Override
	public void completeJob(RobotJob job) {
		// TODO Auto-generated method stub
		
	}
}
