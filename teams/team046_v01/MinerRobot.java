package team046_v01;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;

public class MinerRobot extends BaseRobot {
	
	public MinerRobot(RobotController rc, Strategy strat) {
		super(rc, strat);
	}

	@Override
	public void run() throws GameActionException {

		if (rc.isWeaponReady()) {
			attackSomething();
		}
		
		strat.updateCounts();
		
		if (rc.isCoreReady()) {
			
			if(job == null || job.isCompleted()) {
				job = strat.getNextAssignment();
				rc.setIndicatorString(0, "New Job: " + job);
			}

			if(job.getJobtype() == RobotJob.RobotJobType.MOVE) {
				if(job.getLocation() != null) {
					tryMove(strat.myLocation.directionTo(job.getLocation()));
				} else {
					tryMove(Util.directions[rand.nextInt(8)]);
				}
			} else if(job.getJobtype() == RobotJob.RobotJobType.MINE) {
				rc.mine();
			}
			strat.completeJob(job);
		}
	}

}
