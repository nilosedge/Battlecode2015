package team046_v01;

import java.util.Random;
import java.util.TreeMap;

import battlecode.common.Clock;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import battlecode.common.Team;

public abstract class Strategy {

	protected RobotController rc;
	public static Random rand;
	public Team myTeam;
	public MapLocation myLocation;
	protected MapLocation hqLocation;
	
	public Strategy(RobotController rc) {
		this.rc = rc;
		myTeam = rc.getTeam();
		hqLocation = rc.senseHQLocation();
		rand = new Random(rc.getID());
	}
	
	public abstract void updateCounts();
	public abstract RobotJob getNextAssignment();
	public abstract void completeJob(RobotJob job);
	
	public RobotJob getGlobalJobs() {
		int allIn = 0;
		try {
			allIn = rc.readBroadcast(Util.Channel.ALLIN_OFFSET.getValue());
		} catch (GameActionException e) {
			e.printStackTrace();
		}
		if(allIn != 0) {
			rc.setIndicatorString(2, "Recieved ALLIN moving to HQ");
			RobotJob job = new RobotJob(RobotJob.RobotJobType.MOVE);
			job.setLocation(rc.senseEnemyHQLocation());
			return job;
		} else {
			return null;
		}
		
		// TODO check other battle points and return one of those
		
		// TODO check other HQ jobs
		// TODO Check HQ jobs queue
		
	}
	
	public void sendSuppliesFromBuilding() {
		int mylevel = (int)rc.getSupplyLevel();
		int rounds = (int)Math.floor(Clock.getBytecodesLeft() / 500) - 1;
		
		TreeMap<Integer, MapLocation> nearbyRobots = new TreeMap<Integer, MapLocation>();
		RobotInfo[] myRobots = rc.senseNearbyRobots(35);
		
		for(RobotInfo ri: myRobots) {
			if(Clock.getRoundNum() < 150 || ri.type != RobotType.BEAVER) {
				nearbyRobots.put((int)ri.supplyLevel, ri.location);
			}
		}
		
		for(MapLocation l: nearbyRobots.values()) {
			if(rounds <= 0) break;
			int transamount = (int)Math.ceil(mylevel / rounds);
			try { rc.transferSupplies(transamount, l); } catch (GameActionException e) {}
			rounds--;
		}
	}

}
