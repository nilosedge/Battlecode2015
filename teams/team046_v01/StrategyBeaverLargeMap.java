package team046_v01;

import battlecode.common.RobotController;
import battlecode.common.RobotType;

public class StrategyBeaverLargeMap extends StrategyBeaver {

	public StrategyBeaverLargeMap(RobotController rc) {
		super(rc);
	}

	@Override
	public RobotJob getNextAssignment() {

		if(MINERFACTORY_COUNT < 1) {
			RobotJob job = new RobotJob(RobotJob.RobotJobType.BUILD);
			job.setRobottype(RobotType.MINERFACTORY);
			return job;
		}
		
		if(BARRACKS_COUNT < 1) {
			RobotJob job = new RobotJob(RobotJob.RobotJobType.BUILD);
			job.setRobottype(RobotType.BARRACKS);
			return job;
		}
		
		if(HELIPAD_COUNT < 1 && MINER_COUNT >= 5) {
			RobotJob job = new RobotJob(RobotJob.RobotJobType.BUILD);
			job.setRobottype(RobotType.HELIPAD);
			return job;
		}
		
		if(TANKFACTORY_COUNT < 1 && MINER_COUNT >= 5) {
			RobotJob job = new RobotJob(RobotJob.RobotJobType.BUILD);
			job.setRobottype(RobotType.TANKFACTORY);
			return job;
		}
		
		if(SUPPLYDEPOT_COUNT < 1 && MINER_COUNT >= 10) {
			RobotJob job = new RobotJob(RobotJob.RobotJobType.BUILD);
			job.setRobottype(RobotType.SUPPLYDEPOT);
			return job;
		}
		
		if(TANKFACTORY_COUNT == 1 && BARRACKS_COUNT < 2 && MINER_COUNT >= 6) {
			RobotJob job = new RobotJob(RobotJob.RobotJobType.BUILD);
			job.setRobottype(RobotType.BARRACKS);
			return job;
		}
		
		if(BARRACKS_COUNT == 2 && HELIPAD_COUNT < 2 && MINER_COUNT >= 12) {
			RobotJob job = new RobotJob(RobotJob.RobotJobType.BUILD);
			job.setRobottype(RobotType.HELIPAD);
			return job;
		}
		
		
		
		
		int fate = rand.nextInt(1000);
		if (fate < 600) {
			RobotJob job = new RobotJob(RobotJob.RobotJobType.MINE);
			return job;
		} else {
			RobotJob job = new RobotJob(RobotJob.RobotJobType.MOVE);
			return job;
		}

	}

	@Override
	public void completeJob(RobotJob job) {
		// TODO Auto-generated method stub
		
	}
}
