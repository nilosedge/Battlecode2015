package team046_v01;

import battlecode.common.RobotController;

public class TrainingFieldRobot extends BaseRobot {

	public TrainingFieldRobot(RobotController rc, Strategy strat) {
		super(rc, strat);

	}

	@Override
	public void run() {
		
		strat.sendSuppliesFromBuilding();
	}
}
