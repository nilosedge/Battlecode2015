package team046_v01;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;

public class SpawnBuildingRobot extends BaseRobot {
	
	public SpawnBuildingRobot(RobotController rc, Strategy strat) {
		super(rc, strat);
	}

	@Override
	public void run() throws GameActionException {

		strat.updateCounts();
		
		if (rc.isCoreReady()) {
			
			RobotJob job = strat.getNextAssignment();
			if(job != null && job.getJobtype() == RobotJob.RobotJobType.SPAWN && rc.getTeamOre() > job.getRobottype().oreCost) {
				rc.setIndicatorString(1, "Spawning: " + job.getRobottype());
				trySpawn(Util.directions[rand.nextInt(8)], job.getRobottype());
			} else {
				rc.setIndicatorString(2, "Low resources: " + rc.getTeamOre() + " Job: " + job);
			}
			strat.sendSuppliesFromBuilding();
		}
	}

}
