package team046_v01;

import java.util.ArrayList;
import java.util.TreeMap;

import battlecode.common.Clock;
import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;

public class StrategyHQ extends Strategy {

	protected TreeMap<Integer, MapLocation> nearbyRobots = new TreeMap<Integer, MapLocation>();
	
	protected int AEROSPACELAB_COUNT = 0;
	protected int BARRACKS_COUNT = 0;
	protected int BASHER_COUNT = 0;
	protected int BEAVER_COUNT = 0;
	protected int COMMANDER_COUNT = 0;
	protected int COMPUTER_COUNT = 0;
	protected int DRONE_COUNT = 0;
	protected int HANDWASHSTATION_COUNT = 0;
	protected int HELIPAD_COUNT = 0;
	protected int LAUNCHER_COUNT = 0;
	protected int MINER_COUNT = 0;
	protected int MINERFACTORY_COUNT = 0;
	protected int MISSILE_COUNT = 0;
	protected int SOLDIER_COUNT = 0;
	protected int SUPPLYDEPOT_COUNT = 0;
	protected int TANK_COUNT = 0;
	protected int TANKFACTORY_COUNT = 0;
	protected int TECHNOLOGYINSTITUTE_COUNT = 0;
	protected int TOWER_COUNT = 0;
	protected int TRAININGFIELD_COUNT = 0;

	protected int MAX_BEAVER_COUNT = 0;
	protected ArrayList<MapLocation> minerList = new ArrayList<MapLocation>();
	
	public StrategyHQ(RobotController rc, int count) {
		super(rc);
		updateCounts();
		this.MAX_BEAVER_COUNT  = count;
		myLocation = rc.senseHQLocation();
	}

	@Override
	public void updateCounts() {
		RobotInfo[] myRobots = rc.senseNearbyRobots(999999, myTeam);
		nearbyRobots.clear();
		clearCounts();
		
		for (RobotInfo r : myRobots) {
			RobotType type = r.type;
			if (type == RobotType.AEROSPACELAB) {
				AEROSPACELAB_COUNT++;
			}
			else if (type == RobotType.BARRACKS) {
				BARRACKS_COUNT++;
			}
			else if (type == RobotType.BASHER) {
				BASHER_COUNT++;
			}
			else if (type == RobotType.BEAVER) {
				BEAVER_COUNT++;
			}
			else if (type == RobotType.COMMANDER) {
				COMMANDER_COUNT++;
			}
			else if (type == RobotType.COMPUTER) {
				COMPUTER_COUNT++;
			}
			else if (type == RobotType.DRONE) {
				DRONE_COUNT++;
			}
			else if (type == RobotType.HANDWASHSTATION) {
				HANDWASHSTATION_COUNT++;
			}
			else if (type == RobotType.HELIPAD) {
				HELIPAD_COUNT++;
			}
			else if (type == RobotType.LAUNCHER) {
				LAUNCHER_COUNT++;
			}
			else if (type == RobotType.MINER) {
				if(myLocation.distanceSquaredTo(r.location) > 35) {
					minerList.add(r.location);
				}
				MINER_COUNT++;
			}
			else if (type == RobotType.MINERFACTORY) {
				MINERFACTORY_COUNT++;
			}
			else if (type == RobotType.MISSILE) {
				MISSILE_COUNT++;
			}
			else if (type == RobotType.SOLDIER) {
				SOLDIER_COUNT++;
			}
			else if (type == RobotType.SUPPLYDEPOT) {
				SUPPLYDEPOT_COUNT++;
			}
			else if (type == RobotType.TANK) {
				TANK_COUNT++;
			}
			else if (type == RobotType.TANKFACTORY) {
				TANKFACTORY_COUNT++;
			}
			else if (type == RobotType.TECHNOLOGYINSTITUTE) {
				TECHNOLOGYINSTITUTE_COUNT++;
			}
			else if (type == RobotType.TOWER) {
				TOWER_COUNT++;
			}
			else if (type == RobotType.TRAININGFIELD) {
				TRAININGFIELD_COUNT++;
			}
			
			if(rc.getLocation().distanceSquaredTo(r.location) <= 15) {
				nearbyRobots.put((int)r.supplyLevel, r.location);
			}
		}

		try { rc.broadcast(Util.Channel.AEROSPACELAB_COUNT.getValue(), AEROSPACELAB_COUNT); } catch (GameActionException e) {}
		try { rc.broadcast(Util.Channel.BARRACKS_COUNT.getValue(), BARRACKS_COUNT); } catch (GameActionException e) {}
		try { rc.broadcast(Util.Channel.BASHER_COUNT.getValue(), BASHER_COUNT); } catch (GameActionException e) {}
		try { rc.broadcast(Util.Channel.BEAVER_COUNT.getValue(), BEAVER_COUNT); } catch (GameActionException e) {}
		try { rc.broadcast(Util.Channel.COMMANDER_COUNT.getValue(), COMMANDER_COUNT); } catch (GameActionException e) {}
		try { rc.broadcast(Util.Channel.COMPUTER_COUNT.getValue(), COMPUTER_COUNT); } catch (GameActionException e) {}
		try { rc.broadcast(Util.Channel.DRONE_COUNT.getValue(), DRONE_COUNT); } catch (GameActionException e) {}
		try { rc.broadcast(Util.Channel.HANDWASHSTATION_COUNT.getValue(), HANDWASHSTATION_COUNT); } catch (GameActionException e) {}
		try { rc.broadcast(Util.Channel.HELIPAD_COUNT.getValue(), HELIPAD_COUNT); } catch (GameActionException e) {}
		try { rc.broadcast(Util.Channel.LAUNCHER_COUNT.getValue(), LAUNCHER_COUNT); } catch (GameActionException e) {}
		try { rc.broadcast(Util.Channel.MINER_COUNT.getValue(), MINER_COUNT); } catch (GameActionException e) {}
		try { rc.broadcast(Util.Channel.MINERFACTORY_COUNT.getValue(), MINERFACTORY_COUNT); } catch (GameActionException e) {}
		try { rc.broadcast(Util.Channel.MISSILE_COUNT.getValue(), MISSILE_COUNT); } catch (GameActionException e) {}
		try { rc.broadcast(Util.Channel.SOLDIER_COUNT.getValue(), SOLDIER_COUNT); } catch (GameActionException e) {}
		try { rc.broadcast(Util.Channel.SUPPLYDEPOT_COUNT.getValue(), SUPPLYDEPOT_COUNT); } catch (GameActionException e) {}
		try { rc.broadcast(Util.Channel.TANK_COUNT.getValue(), TANK_COUNT); } catch (GameActionException e) {}
		try { rc.broadcast(Util.Channel.TANKFACTORY_COUNT.getValue(), TANKFACTORY_COUNT); } catch (GameActionException e) {}
		try { rc.broadcast(Util.Channel.TECHNOLOGYINSTITUTE_COUNT.getValue(), TECHNOLOGYINSTITUTE_COUNT); } catch (GameActionException e) {}
		try { rc.broadcast(Util.Channel.TOWER_COUNT.getValue(), TOWER_COUNT); } catch (GameActionException e) {}
		try { rc.broadcast(Util.Channel.TRAININGFIELD_COUNT.getValue(), TRAININGFIELD_COUNT); } catch (GameActionException e) {}

	}

	private void clearCounts() {
		AEROSPACELAB_COUNT = 0;
		BARRACKS_COUNT = 0;
		BASHER_COUNT = 0;
		BEAVER_COUNT = 0;
		COMMANDER_COUNT = 0;
		COMPUTER_COUNT = 0;
		DRONE_COUNT = 0;
		HANDWASHSTATION_COUNT = 0;
		HELIPAD_COUNT = 0;
		LAUNCHER_COUNT = 0;
		MINER_COUNT = 0;
		MINERFACTORY_COUNT = 0;
		MISSILE_COUNT = 0;
		SOLDIER_COUNT = 0;
		SUPPLYDEPOT_COUNT = 0;
		TANK_COUNT = 0;
		TANKFACTORY_COUNT = 0;
		TECHNOLOGYINSTITUTE_COUNT = 0;
		TOWER_COUNT = 0;
		TRAININGFIELD_COUNT = 0;
	}

	public void sendSupplies() {
		int hqlevel = (int)rc.getSupplyLevel();
		int rounds = (int)Math.floor(Clock.getBytecodesLeft() / 500) - 1;
		
		for(MapLocation l: nearbyRobots.values()) {
			if(rounds <= 0) break;
			int transamount = (int)Math.ceil(hqlevel / rounds);
			try { rc.transferSupplies(transamount, l); } catch (GameActionException e) {}
			rounds--;
		}
	}
	

	public RobotJob getNextAssignment() {
		
		if(BEAVER_COUNT < MAX_BEAVER_COUNT && MINER_COUNT >= (BEAVER_COUNT * 5)) {
			RobotJob job = new RobotJob(RobotJob.RobotJobType.SPAWN);
			job.setRobottype(RobotType.BEAVER);
			return job;
		} else {
			return null;
		}
	}
	
	public void setMiningPoints() {
		setMiningPoints(15);
	}
	
	public void setMiningPoints(int distance) {
		Util u = new Util(rc.senseHQLocation());
		
		if(minerList.size() < 5) {
		
			Direction dir = myLocation.directionTo(rc.senseEnemyHQLocation());
			
			int dirint = Util.directionToInt(dir);
			MapLocation ml = rc.getLocation();
			
			MapLocation l = ml.add(Util.directions[(dirint+6)%8], distance);
			MiningLocation m1 = new MiningLocation(l, (int)rc.senseOre(l));
			Util.broadcast(rc, Util.Channel.MININGTOP5_POSITIONS_OFFSET.getValue(), u.convertFromMapLocation(m1));
			
			l = ml.add(Util.directions[(dirint+7)%8], distance);
			MiningLocation m2 = new MiningLocation(l, (int)rc.senseOre(l));
			Util.broadcast(rc, Util.Channel.MININGTOP5_POSITIONS_OFFSET.getValue() + 1, u.convertFromMapLocation(m2));
			
			l = ml.add(Util.directions[(dirint+8)%8], distance);
			MiningLocation m3 = new MiningLocation(l, (int)rc.senseOre(l));
			Util.broadcast(rc, Util.Channel.MININGTOP5_POSITIONS_OFFSET.getValue() + 2, u.convertFromMapLocation(m3));
			
			l = ml.add(Util.directions[(dirint+9)%8], distance);
			MiningLocation m4 = new MiningLocation(l, (int)rc.senseOre(l));
			Util.broadcast(rc, Util.Channel.MININGTOP5_POSITIONS_OFFSET.getValue() + 3, u.convertFromMapLocation(m4));
			
			l = ml.add(Util.directions[(dirint+10)%8], distance);
			MiningLocation m5 = new MiningLocation(l, (int)rc.senseOre(l));
			Util.broadcast(rc, Util.Channel.MININGTOP5_POSITIONS_OFFSET.getValue() + 4, u.convertFromMapLocation(m5));
		
		} else {
			for(int i = 0; i < 5; i++) {
				MapLocation m = minerList.remove(rand.nextInt(minerList.size()));
				MiningLocation ml = new MiningLocation(m, (int)rc.senseOre(m));
				Util.broadcast(rc, Util.Channel.MININGTOP5_POSITIONS_OFFSET.getValue() + i, u.convertFromMapLocation(ml));
			}
		}
	}

	@Override
	public void completeJob(RobotJob job) {
		// TODO Auto-generated method stub
		
	}

}
