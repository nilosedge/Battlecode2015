package team046_v01;

import battlecode.common.RobotType;

public class RobotTypeInfo {

	public RobotType type;
	public int percentage;
	public int count_channel;
	public int max;
	
	public RobotTypeInfo(RobotType type, int percentage, int count_channel, int max) {
		this.type = type;
		this.percentage = percentage;
		this.count_channel = count_channel;
		this.max = max;
	}

}
