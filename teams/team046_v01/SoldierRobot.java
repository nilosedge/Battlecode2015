package team046_v01;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;

public class SoldierRobot extends BaseRobot {

	public SoldierRobot(RobotController rc, Strategy strat) {
		super(rc, strat);
		strat = StrategyManager.createStrategy(rc);
	}

	@Override
	public void run() throws GameActionException {

		if (rc.isWeaponReady()) {
			attackSomething();
		}
	
		strat.updateCounts();
		
		if (rc.isCoreReady()) {
			
			job = strat.getNextAssignment();
			rc.setIndicatorString(0, "Job: " + job);
			
			if(job.getJobtype() == RobotJob.RobotJobType.MOVE) {
				tryMove(strat.myLocation.directionTo(job.getLocation()));
			} else if(job.getJobtype() == RobotJob.RobotJobType.PATROL) {
				if(strat.myLocation.distanceSquaredTo(job.getLocation()) > 15) {
					tryMove(strat.myLocation.directionTo(job.getLocation()));
				} else {
					tryMove(Util.directions[rand.nextInt(8)]);
				}
			} else {
				tryMove(Util.directions[rand.nextInt(8)]);
			}

		}
	}
}
