package team046_v01;

import java.util.TreeMap;

import battlecode.common.MapLocation;
import battlecode.common.RobotController;

public class StrategySoldier extends Strategy {

	public StrategySoldier(RobotController rc) {
		super(rc);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void updateCounts() {
		myLocation = rc.getLocation();
		
	}

	@Override
	public RobotJob getNextAssignment() {
		
		RobotJob globalJob = getGlobalJobs();
		if(globalJob != null) {
			return globalJob;
		}
		
		MapLocation[] towers = rc.senseTowerLocations();
		
		TreeMap<Integer, MapLocation> map = new TreeMap<Integer, MapLocation>();
		
		for(MapLocation m: towers) {
			map.put(myLocation.distanceSquaredTo(m), m);
		}
		
		RobotJob job = new RobotJob(RobotJob.RobotJobType.PATROL);
		job.setLocation(map.firstEntry().getValue());
		return job;
		
	}

	@Override
	public void completeJob(RobotJob job) {
		// TODO Auto-generated method stub
		
	}

}
