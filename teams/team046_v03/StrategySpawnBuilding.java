package team046_v03;

import java.util.ArrayList;

public class StrategySpawnBuilding extends Strategy {

	private ArrayList<RobotTypeInfo> buildTypes = new ArrayList<RobotTypeInfo>();
	
	public StrategySpawnBuilding(RobotSetup rs) {
		super(rs);
	}

	@Override
	public void updateCounts() {
		for(RobotTypeInfo type: buildTypes) {
			type.cinfo = bc.readBroadcastCount(type.count_channel);
		}
		teamOre = rc.getTeamOre();
	}

	@Override
	public RobotJob getNextAssignment() {
		
		int fate = rand.nextInt(10000);
		
		for(RobotTypeInfo type: buildTypes) {

			if(type.cinfo.currentCount < type.cinfo.maxCount && fate <= (type.percentage * 100)) {
				RobotJob job = new RobotJob(RobotJob.RobotJobType.SPAWN);
				job.setRobottype(type.robotType);
				return job;
			}
		}
		return null;
	}

	@Override
	public void completeJob(RobotJob job) {
		// TODO Auto-generated method stub
		
	}

	public void addBuildingType(RobotTypeInfo robotTypeInfo) {
		buildTypes.add(robotTypeInfo);
	}

}
