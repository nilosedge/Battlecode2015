package team046_v03;

import battlecode.common.GameActionException;

public class LauncherRobot extends BaseRobot {

	public LauncherRobot(RobotSetup rs) {
		super(rs);
	}

	@Override
	public void run() throws GameActionException {
		
		strat.sendSupplies();
		
		strat.updateCounts();
		
		if (rc.isCoreReady()) {
			
			if(job == null || job.isCompleted()) {
				job = strat.getNextAssignment();
				rc.setIndicatorString(0, "New Job: " + job);
			}
			
			if(job != null && job.getJobtype() == RobotJob.RobotJobType.MOVE) {
				tryMove(strat.myLocation.directionTo(job.getLocation()));
			} else if(job != null && job.getJobtype() == RobotJob.RobotJobType.PATROL) {
				if(strat.myLocation.distanceSquaredTo(job.getLocation()) > 15) {
					tryMove(strat.myLocation.directionTo(job.getLocation()));
				} else {
					tryMove(Util.directions[rand.nextInt(8)]);
				}
			} else {
				tryMove(Util.directions[rand.nextInt(8)]);
			}
			rc.setIndicatorString(1, "Missiles: " + rc.getMissileCount());
			strat.completeJob(job);
		}

	}

}
