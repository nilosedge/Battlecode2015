package team046_v03;

import java.util.Random;

import battlecode.common.GameActionException;
import battlecode.common.GameConstants;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.Team;

public abstract class Strategy {

	protected RobotController rc;
	public static Random rand;
	public Team myTeam;
	public double teamOre;
	public MapLocation myLocation;
	protected MapLocation hqLocation;
	protected BroadcastSytem bc;
	
	public Strategy(RobotSetup rs) {
		this.rc = rs.rc;
		this.bc = rs.bc;
		myTeam = rc.getTeam();
		hqLocation = rc.senseHQLocation();
		rand = new Random(rc.getID());
	}
	
	public abstract void updateCounts();
	public abstract RobotJob getNextAssignment();
	public abstract void completeJob(RobotJob job);
	//public abstract ArrayList<RobotTypeInfo> getBuildList();
	
	public RobotJob getGlobalJobs() {
		int allIn = 0;
		try {
			allIn = rc.readBroadcast(Util.Channel.ALLIN_OFFSET.getValue());
		} catch (GameActionException e) {
			e.printStackTrace();
		}
		if(allIn != 0) {
			rc.setIndicatorString(2, "Recieved ALLIN moving to HQ");
			RobotJob job = new RobotJob(RobotJob.RobotJobType.MOVE);
			job.setLocation(rc.senseEnemyHQLocation());
			return job;
		} else {
			return null;
		}
		
		// TODO check other battle points and return one of those
		
		// TODO check other HQ jobs
		// TODO Check HQ jobs queue
		
	}
	
	public void sendSupplies() {
		double mylevel = rc.getSupplyLevel();
		if(mylevel < 500) return;
		RobotInfo[] myRobots = rc.senseNearbyRobots(GameConstants.SUPPLY_TRANSFER_RADIUS_SQUARED, myTeam);
		double lowest = mylevel;
		RobotInfo supplyRobot = null;
		for(RobotInfo ri: myRobots) {
			if(ri.supplyLevel < lowest) {
				supplyRobot = ri;
				lowest = ri.supplyLevel;
			}
		}
		if(supplyRobot != null) {
			try { rc.transferSupplies((int)((mylevel - supplyRobot.supplyLevel) / 2), supplyRobot.location); } catch (GameActionException e) { e.printStackTrace(); }
		}
	}

	public int readBroadcast(Util.Channel chan) {
		try {
			return rc.readBroadcast(chan.getValue());
		} catch (GameActionException e) { e.printStackTrace(); }
		return 0;
	}

}
