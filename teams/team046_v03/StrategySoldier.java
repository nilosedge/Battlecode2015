package team046_v03;

import java.util.TreeMap;

import battlecode.common.MapLocation;

public class StrategySoldier extends Strategy {

	public StrategySoldier(RobotSetup rs) {
		super(rs);
	}

	@Override
	public void updateCounts() {
		myLocation = rc.getLocation();
	}

	@Override
	public RobotJob getNextAssignment() {
		
		RobotJob globalJob = getGlobalJobs();
		if(globalJob != null) {
			return globalJob;
		}
		
		MapLocation[] towers = rc.senseTowerLocations();
		
		TreeMap<Integer, MapLocation> map = new TreeMap<Integer, MapLocation>();
		
		for(MapLocation m: towers) {
			map.put(myLocation.distanceSquaredTo(m), m);
		}
		
		RobotJob job = new RobotJob(RobotJob.RobotJobType.PATROL);
		job.setLocation(map.firstEntry().getValue());
		return job;
		
	}

	@Override
	public void completeJob(RobotJob job) {
		// TODO Auto-generated method stub
		
	}

}
