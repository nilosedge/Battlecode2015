package team046_v03;

public class RobotManager {
	
	public static BaseRobot createRobot(RobotSetup rs) {
		
		switch(rs.rc.getType()) {
			case HQ:
				return new HQRobot(rs);
			case AEROSPACELAB:
				return new SpawnBuildingRobot(rs);
			case BARRACKS:
				return new SpawnBuildingRobot(rs);
			case BASHER:
				return new BasherRobot(rs);
			case BEAVER:
				return new BeaverRobot(rs);
			case COMMANDER:
				return new CommanderRobot(rs);
			case COMPUTER:
				return new ComputerRobot(rs);
			case DRONE:
				return new DroneRobot(rs);
			case HANDWASHSTATION:
				return new HandwashingStationRobot(rs);
			case HELIPAD:
				return new SpawnBuildingRobot(rs);
			case LAUNCHER:
				return new LauncherRobot(rs);
			case MINER:
				return new MinerRobot(rs);
			case MINERFACTORY:
				return new SpawnBuildingRobot(rs);
			case MISSILE:
				return new MissleRobot(rs);
			case SOLDIER:
				return new SoldierRobot(rs);
			case SUPPLYDEPOT:
				return new SupplyDepotRobot(rs);
			case TANK:
				return new TankRobot(rs);
			case TANKFACTORY:
				return new SpawnBuildingRobot(rs);
			case TECHNOLOGYINSTITUTE:
				return new SpawnBuildingRobot(rs);
			case TOWER:
				return new TowerRobot(rs);
			case TRAININGFIELD:
				return new SpawnBuildingRobot(rs);
		}
		return null;
	}

}
