package team046_v03;

import battlecode.common.MapLocation;

public class StrategyDrone extends Strategy {

	public StrategyDrone(RobotSetup rs) {
		super(rs);
	}

	@Override
	public void updateCounts() {
		myLocation = rc.getLocation();
	}

	@Override
	public RobotJob getNextAssignment() {
		
		RobotJob globalJob = getGlobalJobs();
		if(globalJob != null) {
			return globalJob;
		}
		
		MapLocation[] towers = rc.senseTowerLocations();
		
//		TreeMap<Integer, MapLocation> map = new TreeMap<Integer, MapLocation>();
//		
//		for(MapLocation m: towers) {
//			map.put(myLocation.distanceSquaredTo(m), m);
//		}
//		
//		RobotJob job = new RobotJob(RobotJobType.PATROL);
//		job.setLocation(map.firstEntry().getValue());
//		return job;
		
		if(towers.length > 0) {
			int fate = rand.nextInt(towers.length);
			
			RobotJob job = new RobotJob(RobotJob.RobotJobType.PATROL);
			job.setLocation(towers[fate]);
			return job;
		} else {
			return null;
		}
	}
	
	public void completeJob(RobotJob job) {
		if((job.getJobtype() == RobotJob.RobotJobType.MOVE || job.getJobtype() == RobotJob.RobotJobType.PATROL) && (job.getLocation() == null || job.getLocation().distanceSquaredTo(myLocation) <= 16)) {
			job.setCompleted(true);
		}
	}
}
