package team046_v03;

import battlecode.common.Clock;
import battlecode.common.GameActionException;

public class MinerRobot extends BaseRobot {
	
	public MinerRobot(RobotSetup rs) {
		super(rs);
	}

	@Override
	public void run() throws GameActionException {

		strat.sendSupplies();
		
		if (rc.isWeaponReady()) {
			attackSomething();
		}
		
		strat.updateCounts();
		
		if (rc.isCoreReady()) {
			
			if(job == null || job.isCompleted()) {
				job = strat.getNextAssignment();
				rc.setIndicatorString(0, "New Job: " + job + " Round: " + Clock.getRoundNum());
			}

			if(job.getJobtype() == RobotJob.RobotJobType.MOVE) {
				if(job.getLocation() != null) {
					tryMove(strat.myLocation.directionTo(job.getLocation()));
				} else {
					tryMove(Util.directions[rand.nextInt(8)]);
				}
			} else if(job.getJobtype() == RobotJob.RobotJobType.MINE) {
				rc.mine();
			}
			strat.completeJob(job);
		}
	}

}
