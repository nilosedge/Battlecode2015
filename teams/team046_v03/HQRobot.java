package team046_v03;

import battlecode.common.Clock;
import battlecode.common.GameActionException;

public class HQRobot extends BaseRobot {

	public HQRobot(RobotSetup rs) {
		super(rs);
	}

	public void run() throws GameActionException {

		strat.sendSupplies();
		
		if (rc.isWeaponReady()) {
			attackSomething();
		}

		strat.updateCounts();
		if(Clock.getRoundNum() % 55 == 0) {
			((StrategyHQ)strat).setMiningPoints();
		}

		//RobotJob[] jobs = strat.getGlobalJobs();
		
		if (rc.isCoreReady()) {
			RobotJob job = strat.getNextAssignment();
			if(job != null && job.getJobtype() == RobotJob.RobotJobType.SPAWN && (rc.getTeamOre() - 5) >= job.getRobottype().oreCost) {
				trySpawn(Util.directions[rand.nextInt(8)], job.getRobottype());
			}
			
			rc.setIndicatorString(0, "Total Ore: " + rc.getTeamOre());
		}

		// Large Map 1600
		// Medium Map 1700
		// Small Map ??
		if(Util.Channel.DRONE_COUNTS.getInfo().currentCount > 30) {
			bc.sendAllInBroadCast();
		} else {
			bc.clearAllInBroadCast();
		}
		if(Clock.getRoundNum() >= 1600) {
			bc.sendAllInBroadCast();
		}
		
	}

}