package team046_v03;

import battlecode.common.MapLocation;

public class StrategyBasher extends Strategy {

	public StrategyBasher(RobotSetup rs) {
		super(rs);
	}

	@Override
	public void updateCounts() {
		myLocation = rc.getLocation();
	}

	@Override
	public RobotJob getNextAssignment() {
		
		RobotJob globalJob = getGlobalJobs();
		if(globalJob != null) {
			return globalJob;
		}
		
		MapLocation[] towers = rc.senseTowerLocations();
		
//		TreeMap<Integer, MapLocation> map = new TreeMap<Integer, MapLocation>();
//		
//		for(MapLocation m: towers) {
//			map.put(myLocation.distanceSquaredTo(m), m);
//		}
//		
//		RobotJob job = new RobotJob(RobotJobType.PATROL);
//		job.setLocation(map.firstEntry().getValue());
//		return job;
		
		
		int fate = rand.nextInt(towers.length);
		
		RobotJob job = new RobotJob(RobotJob.RobotJobType.PATROL);
		job.setLocation(towers[fate]);
		return job;

	}
	
	public void completeJob(RobotJob job) {
		if((job.getJobtype() == RobotJob.RobotJobType.MOVE || job.getJobtype() == RobotJob.RobotJobType.PATROL) && (job.getLocation() == null || job.getLocation().distanceSquaredTo(myLocation) <= 16)) {
			job.setCompleted(true);
		}
	}
}
