package team046_v03;

import battlecode.common.GameActionException;

public class SpawnBuildingRobot extends BaseRobot {
	
	public SpawnBuildingRobot(RobotSetup rs) {
		super(rs);
	}

	@Override
	public void run() throws GameActionException {

		strat.sendSupplies();
		
		strat.updateCounts();
		
		if (rc.isCoreReady()) {
			
			RobotJob job = strat.getNextAssignment();
			if(job != null && job.getJobtype() == RobotJob.RobotJobType.SPAWN &&
					strat.teamOre > job.getRobottype().oreCost) {
				rc.setIndicatorString(1, "Spawning: " + job.getRobottype());
				trySpawn(Util.directions[rand.nextInt(8)], job.getRobottype());
			} else {
				rc.setIndicatorString(2, "Low resources: " + rc.getTeamOre() + " Job: " + job);
			}
		}
		
	}

}
