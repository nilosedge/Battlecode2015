package team046_v03;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;

public class BroadcastSytem {

	private RobotController rc;
	private Util u;
	
	public BroadcastSytem(RobotController rc) {
		this.rc = rc;
		u = new Util(rc.senseHQLocation());
	}

	public CountInfo readBroadcastCount(Util.Channel chan) {
		return u.convertCountMessage(readBroadcast(chan));
	}
	
	public void broadcastCount(Util.Channel chan, CountInfo info) {
		broadcast(chan, u.convertCountMessage(info));
	}
	
	public int readBroadcast(Util.Channel chan) {
		try {
			return rc.readBroadcast(chan.getValue());
		} catch (GameActionException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public void broadcast(Util.Channel chan, int data) {
		try {
			rc.broadcast(chan.getValue(), data);
		} catch (GameActionException e) {
			e.printStackTrace();
		}
	}

	public void sendAllInBroadCast() {
		rc.setIndicatorString(1, "Sending ALLIN Message");
		broadcast(Util.Channel.ALLIN_OFFSET, 1);
	}

	public void clearAllInBroadCast() {
		rc.setIndicatorString(1, "Clearing ALLIN Message");
		broadcast(Util.Channel.ALLIN_OFFSET, 0);
	}
}
