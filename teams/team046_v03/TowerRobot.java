package team046_v03;

import battlecode.common.GameActionException;

public class TowerRobot extends BaseRobot {

	public TowerRobot(RobotSetup rs) {
		super(rs);
	}

	@Override
	public void run() throws GameActionException {

		strat.sendSupplies();
		
		if (rc.isWeaponReady()) {
			attackSomething();
		}

	}

}
