package team046_v03;

public class StrategyBeaver extends Strategy {

	protected CountInfo supplyDepotCount;

	protected Util.Channel[] buildingList = {
		Util.Channel.MINERFACTORY_COUNTS,
		Util.Channel.HELIPAD_COUNTS,
		Util.Channel.SUPPLYDEPOT_COUNTS,
		Util.Channel.BARRACKS_COUNTS,
		Util.Channel.TANKFACTORY_COUNTS,
		Util.Channel.TECHNOLOGYINSTITUTE_COUNTS,
		Util.Channel.HANDWASHSTATION_COUNTS,
		Util.Channel.TRAININGFIELD_COUNTS,
		Util.Channel.AEROSPACELAB_COUNTS
	};
	
	public StrategyBeaver(RobotSetup rs) {
		super(rs);
	}
	
	public void updateCounts() {
		myLocation = rc.getLocation();
		teamOre = rc.getTeamOre();
	}

	public RobotJob getNextAssignment() {

		CountInfo ci = null;
		for(Util.Channel c: buildingList) {
			ci = bc.readBroadcastCount(c);
			if(ci.currentCount < ci.maxCount) {
				if(teamOre >= c.getRobotType().oreCost) {
					RobotJob job = new RobotJob(RobotJob.RobotJobType.BUILD);
					job.setRobottype(c.getRobotType());
					return job;
				} else {
					break;
				}
			}
		}
	
		return null;
	}

	@Override
	public void completeJob(RobotJob job) {
		
		
	}
}
