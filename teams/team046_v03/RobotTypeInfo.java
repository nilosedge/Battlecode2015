package team046_v03;

import battlecode.common.RobotType;

public class RobotTypeInfo {

	public RobotType robotType;
	public int percentage;
	public Util.Channel count_channel;
	public CountInfo cinfo;
	
	public RobotTypeInfo(RobotType robotType, int percentage, Util.Channel count_channel) {
		this.robotType = robotType;
		this.percentage = percentage;
		this.count_channel = count_channel;
	}


}
