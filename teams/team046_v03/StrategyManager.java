package team046_v03;

import battlecode.common.RobotType;

public class StrategyManager {

	
	public static Strategy createStrategy(RobotSetup rs) {
		
		Strategy strat = null;
		if(rs.rc.getType() == RobotType.AEROSPACELAB) {
			strat = new StrategySpawnBuilding(rs);
			((StrategySpawnBuilding)strat).addBuildingType(new RobotTypeInfo(RobotType.LAUNCHER, 100, Util.Channel.LAUNCHER_COUNTS));
		} else if(rs.rc.getType() == RobotType.BARRACKS) {
			strat = new StrategySpawnBuilding(rs);
			((StrategySpawnBuilding)strat).addBuildingType(new RobotTypeInfo(RobotType.BASHER, 60, Util.Channel.BASHER_COUNTS));
			((StrategySpawnBuilding)strat).addBuildingType(new RobotTypeInfo(RobotType.SOLDIER, 40, Util.Channel.SOLDIER_COUNTS));
		} else if(rs.rc.getType() == RobotType.BASHER) {
			strat = new StrategyBasher(rs);
		} else if(rs.rc.getType() == RobotType.BEAVER) {
			//strat = new BeaverBuildingStrategySmallMap(rc);
			//strat = new BeaverBuildingStrategyMediumMap(rc);
			strat = new StrategyBeaver(rs);
		} else if(rs.rc.getType() == RobotType.COMMANDER) {
		} else if(rs.rc.getType() == RobotType.COMPUTER) {
		} else if(rs.rc.getType() == RobotType.DRONE) {
			strat = new StrategyDrone(rs);
		} else if(rs.rc.getType() == RobotType.HANDWASHSTATION) {
			strat = new StrategyDoNothing(rs);
		} else if(rs.rc.getType() == RobotType.HELIPAD) {
			// Large Map 100
			// Medium 40
			// Small 20
			strat = new StrategySpawnBuilding(rs);
			((StrategySpawnBuilding)strat).addBuildingType(new RobotTypeInfo(RobotType.DRONE, 100, Util.Channel.DRONE_COUNTS));
		} else if(rs.rc.getType() == RobotType.HQ) {
			//strat = new HQStrategyMediumMap(rc);
			//strat = new HQStrategySmallMap(rc);
			//strat = new StrategyHQ(rs, 2);
			
			strat = new StrategyHQ(rs, 1);
			((StrategySpawnBuilding)strat).addBuildingType(new RobotTypeInfo(RobotType.BEAVER, 100, Util.Channel.BEAVER_COUNTS));
		} else if(rs.rc.getType() == RobotType.LAUNCHER) {
			strat = new StrategyLauncher(rs);
		} else if(rs.rc.getType() == RobotType.MINER) {
			//strat = new MinerStrategySmallMap(rc);
			//strat = new MinerStrategyMediumMap(rc);
			// Large mineLowAmount = 12;
			// Medium mineLowAmount = 8;
			// Small mineLowAmount = 4;
			strat = new StrategyMiner(rs, 8);
		} else if(rs.rc.getType() == RobotType.MINERFACTORY) {
			// Large 40
			// Medium 20
			// Small 5
			strat = new StrategySpawnBuilding(rs);
			((StrategySpawnBuilding)strat).addBuildingType(new RobotTypeInfo(RobotType.MINER, 100, Util.Channel.MINER_COUNTS));
		} else if(rs.rc.getType() == RobotType.MISSILE) {
		} else if(rs.rc.getType() == RobotType.SOLDIER) {
			strat = new StrategySoldier(rs);
		} else if(rs.rc.getType() == RobotType.SUPPLYDEPOT) {
			strat = new StrategyDoNothing(rs);
		} else if(rs.rc.getType() == RobotType.TANK) {
			strat = new StrategyTank(rs);
		} else if(rs.rc.getType() == RobotType.TANKFACTORY) {
			// Large 80?
			// Medium 20
			// Small 5
			strat = new StrategySpawnBuilding(rs);
			((StrategySpawnBuilding)strat).addBuildingType(new RobotTypeInfo(RobotType.TANK, 100, Util.Channel.TANK_COUNTS));
		} else if(rs.rc.getType() == RobotType.TECHNOLOGYINSTITUTE) {
			strat = new StrategySpawnBuilding(rs);
			((StrategySpawnBuilding)strat).addBuildingType(new RobotTypeInfo(RobotType.COMPUTER, 100, Util.Channel.COMPUTER_COUNTS));
			strat = new StrategySpawnBuilding(rs);
		} else if(rs.rc.getType() == RobotType.TOWER) {
			strat = new StrategyDoNothing(rs);
		} else if(rs.rc.getType() == RobotType.TRAININGFIELD) {
			strat = new StrategySpawnBuilding(rs);
			((StrategySpawnBuilding)strat).addBuildingType(new RobotTypeInfo(RobotType.COMMANDER, 100, Util.Channel.COMMANDER_COUNTS));
		}

		return strat;
		
	}

}
