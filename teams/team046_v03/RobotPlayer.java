package team046_v03;

import battlecode.common.RobotController;


public class RobotPlayer {
	
	public static void run(RobotController rc) {
		RobotSetup rs = new RobotSetup();
		rs.rc = rc;
		BroadcastSytem bc = new BroadcastSytem(rc);
		rs.bc = bc;
		try {
			rs.strat = StrategyManager.createStrategy(rs);
			rs.robot = RobotManager.createRobot(rs);
			rs.robot.loop();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
