package team046_v03;

import battlecode.common.Direction;
import battlecode.common.MapLocation;
import battlecode.common.RobotType;

public class Util {
	private MapLocation hq;
	public static Direction[] directions = {Direction.NORTH, Direction.NORTH_EAST, Direction.EAST, Direction.SOUTH_EAST, Direction.SOUTH, Direction.SOUTH_WEST, Direction.WEST, Direction.NORTH_WEST};
	public int mask = 0x03FF;
	
	public Util(MapLocation hq) {
		this.hq = hq;
	}
	
	public int convertFromMapLocation(MiningLocation location) {
		location.x = location.x - hq.x;
		location.y = location.y - hq.y;
		return ((((location.x & mask) << 10) | (location.y & mask)) << 10) | (location.amount & mask);
	}

	public MiningLocation convertToMiningLocation(int read) {

		if(read == 0) {
			return null;
		} else {
			int amount = (read & mask);
			String vs1 = Integer.toBinaryString(((read >>> 10) & mask));
			if(vs1.startsWith("1") && vs1.length() == 10) { vs1 = "111111" + vs1; }
			int y = Integer.valueOf(vs1, 2).shortValue();
			vs1 = Integer.toBinaryString(((read >>> 20) & mask));
			if(vs1.startsWith("1") && vs1.length() == 10) { vs1 = "111111" + vs1; }
			int x = Integer.valueOf(vs1, 2).shortValue();
			return new MiningLocation(x + hq.x, y + hq.y, amount);
		}
	}
	
	public CountInfo convertCountMessage(int read) {
		CountInfo ret = new CountInfo();
		ret.buildCount = (read & mask);
		ret.maxCount = (read >>> 10) & mask;
		ret.currentCount = (read >>> 20) & mask;
		return ret;
	}

	public int convertCountMessage(CountInfo info) {
		return ((((info.currentCount & mask) << 10) | (info.maxCount & mask)) << 10) | (info.buildCount & mask);
	}
	
	static int directionToInt(Direction d) {
		switch(d) {
			case NORTH:
				return 0;
			case NORTH_EAST:
				return 1;
			case EAST:
				return 2;
			case SOUTH_EAST:
				return 3;
			case SOUTH:
				return 4;
			case SOUTH_WEST:
				return 5;
			case WEST:
				return 6;
			case NORTH_WEST:
				return 7;
			default:
				return -1;
		}
	}
	
	public enum Channel {
		
		AEROSPACELAB_COUNTS(0, RobotType.AEROSPACELAB, new CountInfo()),
		BARRACKS_COUNTS(1, RobotType.BARRACKS, new CountInfo()),
		BASHER_COUNTS(2, RobotType.BASHER, new CountInfo()),
		BEAVER_COUNTS(3, RobotType.BEAVER, new CountInfo()),
		COMMANDER_COUNTS(4, RobotType.COMMANDER, new CountInfo()),
		COMPUTER_COUNTS(5, RobotType.COMPUTER, new CountInfo()),
		DRONE_COUNTS(6, RobotType.DRONE, new CountInfo()),
		HANDWASHSTATION_COUNTS(7, RobotType.HANDWASHSTATION, new CountInfo()),
		HELIPAD_COUNTS(8, RobotType.HELIPAD, new CountInfo()),
		LAUNCHER_COUNTS(9, RobotType.LAUNCHER, new CountInfo()),
		MINER_COUNTS(10, RobotType.MINER, new CountInfo()),
		MINERFACTORY_COUNTS(11, RobotType.MINERFACTORY, new CountInfo()),
		SOLDIER_COUNTS(12, RobotType.SOLDIER, new CountInfo()),
		SUPPLYDEPOT_COUNTS(13, RobotType.SUPPLYDEPOT, new CountInfo()),
		TANK_COUNTS(14, RobotType.TANK, new CountInfo()),
		TANKFACTORY_COUNTS(15, RobotType.TANKFACTORY, new CountInfo()),
		TECHNOLOGYINSTITUTE_COUNTS(16, RobotType.TECHNOLOGYINSTITUTE, new CountInfo()),
		TOWER_COUNTS(17, RobotType.TOWER, new CountInfo()),
		TRAININGFIELD_COUNTS(18, RobotType.TRAININGFIELD, new CountInfo()),
		
		MININGTOP5_POSITIONS_OFFSET1(100, null, null),
		MININGTOP5_POSITIONS_OFFSET2(101, null, null),
		MININGTOP5_POSITIONS_OFFSET3(102, null, null),
		MININGTOP5_POSITIONS_OFFSET4(103, null, null),
		MININGTOP5_POSITIONS_OFFSET5(104, null, null),
		ALLIN_OFFSET(200, null, null),
		;
		
		private int value;
		private RobotType robotType;
		private CountInfo info;

		private Channel(int value, RobotType robotType, CountInfo info) {
			this.value = value;
			this.robotType = robotType;
			this.info = info;
		}
		
		public int getValue() {
			return value;
		}
		public RobotType getRobotType() {
			return robotType;
		}
		public CountInfo getInfo() {
			return info;
		}
	}
}
