package team046_v03;

import java.util.ArrayList;

import battlecode.common.Direction;
import battlecode.common.MapLocation;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;

public class StrategyHQ extends StrategySpawnBuilding {
	
	protected ArrayList<MapLocation> minerList = new ArrayList<MapLocation>();
	
	public StrategyHQ(RobotSetup rs, int count) {
		super(rs);
		myLocation = rc.senseHQLocation();
		CountInfo i = new CountInfo();
		i.maxCount = count;
		bc.broadcastCount(Util.Channel.BEAVER_COUNTS, i);
		Util.Channel.BEAVER_COUNTS.getInfo().maxCount = 1;
		Util.Channel.MINERFACTORY_COUNTS.getInfo().maxCount = 1;
		Util.Channel.HELIPAD_COUNTS.getInfo().maxCount = 5;
		Util.Channel.SUPPLYDEPOT_COUNTS.getInfo().maxCount = 30;
		Util.Channel.MINER_COUNTS.getInfo().maxCount = 20;
		Util.Channel.DRONE_COUNTS.getInfo().maxCount = 40;
	}
	
	private void clearCounts() {
		for(Util.Channel c: Util.Channel.values()) {
			if(c.getRobotType() != null) {
				Util.Channel.valueOf((c.getRobotType().toString() + "_COUNTS")).getInfo().currentCount = 0;
			}
		}
	}

	@Override
	public void updateCounts() {
		super.updateCounts();
		RobotInfo[] myRobots = rc.senseNearbyRobots(999999, myTeam);
		clearCounts();

		for (RobotInfo r : myRobots) {
			Util.Channel.valueOf((r.type.toString() + "_COUNTS")).getInfo().currentCount++;
			if(r.type == RobotType.MINER) {
				minerList.add(r.location);
			}
		}
		
		for(Util.Channel c: Util.Channel.values()) {
			if(c.getRobotType() != null) {
				bc.broadcastCount(c, c.getInfo());
			}
		}

	}
	
	public void setMiningPoints() {
		setMiningPoints(15);
	}
	
	public void setMiningPoints(int distance) {
		Util u = new Util(rc.senseHQLocation());
		
		if(minerList.size() < 5) {

			Direction dir = myLocation.directionTo(rc.senseEnemyHQLocation());
			
			int dirint = Util.directionToInt(dir);
			MapLocation ml = rc.getLocation();
			
			MapLocation l = ml.add(Util.directions[(dirint+6)%8], distance);
			MiningLocation m1 = new MiningLocation(l, (int)rc.senseOre(l));
			bc.broadcast(Util.Channel.MININGTOP5_POSITIONS_OFFSET1, u.convertFromMapLocation(m1));
			
			l = ml.add(Util.directions[(dirint+7)%8], distance);
			MiningLocation m2 = new MiningLocation(l, (int)rc.senseOre(l));
			bc.broadcast(Util.Channel.MININGTOP5_POSITIONS_OFFSET2, u.convertFromMapLocation(m2));
			
			l = ml.add(Util.directions[(dirint+8)%8], distance);
			MiningLocation m3 = new MiningLocation(l, (int)rc.senseOre(l));
			bc.broadcast(Util.Channel.MININGTOP5_POSITIONS_OFFSET3, u.convertFromMapLocation(m3));
			
			l = ml.add(Util.directions[(dirint+9)%8], distance);
			MiningLocation m4 = new MiningLocation(l, (int)rc.senseOre(l));
			bc.broadcast(Util.Channel.MININGTOP5_POSITIONS_OFFSET4, u.convertFromMapLocation(m4));
			
			l = ml.add(Util.directions[(dirint+10)%8], distance);
			MiningLocation m5 = new MiningLocation(l, (int)rc.senseOre(l));
			bc.broadcast(Util.Channel.MININGTOP5_POSITIONS_OFFSET5, u.convertFromMapLocation(m5));
			
		} else {

			MapLocation m = minerList.remove(rand.nextInt(minerList.size()));
			MiningLocation ml = new MiningLocation(m, (int)rc.senseOre(m));
			bc.broadcast(Util.Channel.MININGTOP5_POSITIONS_OFFSET1, u.convertFromMapLocation(ml));
			
			m = minerList.remove(rand.nextInt(minerList.size()));
			ml = new MiningLocation(m, (int)rc.senseOre(m));
			bc.broadcast(Util.Channel.MININGTOP5_POSITIONS_OFFSET2, u.convertFromMapLocation(ml));
			
			m = minerList.remove(rand.nextInt(minerList.size()));
			ml = new MiningLocation(m, (int)rc.senseOre(m));
			bc.broadcast(Util.Channel.MININGTOP5_POSITIONS_OFFSET3, u.convertFromMapLocation(ml));
		
			m = minerList.remove(rand.nextInt(minerList.size()));
			ml = new MiningLocation(m, (int)rc.senseOre(m));
			bc.broadcast(Util.Channel.MININGTOP5_POSITIONS_OFFSET4, u.convertFromMapLocation(ml));
			
			m = minerList.remove(rand.nextInt(minerList.size()));
			ml = new MiningLocation(m, (int)rc.senseOre(m));
			bc.broadcast(Util.Channel.MININGTOP5_POSITIONS_OFFSET5, u.convertFromMapLocation(ml));
				
		}
	}

	@Override
	public void completeJob(RobotJob job) {
		// TODO Auto-generated method stub
		
	}

}
