package team046_v03;

import battlecode.common.GameActionException;

public class DroneRobot extends BaseRobot {

	public DroneRobot(RobotSetup rs) {
		super(rs);
	}

	@Override
	public void run() throws GameActionException {
		strat.sendSupplies();
		
		strat.updateCounts();
		
		if (rc.isWeaponReady()) {
			attackSomething();
		}
		
		if (rc.isCoreReady()) {
			
			if(job == null || job.isCompleted()) {
				job = strat.getNextAssignment();
				rc.setIndicatorString(0, "New Job: " + job);
			}
			
			if(job.getJobtype() == RobotJob.RobotJobType.MOVE) {
				tryMove(strat.myLocation.directionTo(job.getLocation()));
			} else if(job.getJobtype() == RobotJob.RobotJobType.PATROL) {
				if(strat.myLocation.distanceSquaredTo(job.getLocation()) > 15) {
					tryMove(strat.myLocation.directionTo(job.getLocation()));
				} else {
					tryMove(Util.directions[rand.nextInt(8)]);
				}
			} else {
				tryMove(Util.directions[rand.nextInt(8)]);
			}
			strat.completeJob(job);
		}

	}
	
}
