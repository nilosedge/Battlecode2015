package team046_v05;

public class CountInfo {

	public CountInfo(int currentCount, int maxCount, int buildCount) {
		this.currentCount = currentCount;
		this.maxCount = maxCount;
		this.buildCount = buildCount;
	}
	
	public int currentCount;
	public int buildCount;
	public int maxCount;
}
