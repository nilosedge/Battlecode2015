package team046_v05;

import battlecode.common.Direction;
import battlecode.common.MapLocation;

public class Util {
	private MapLocation hq;
	public static Direction[] directions = {Direction.NORTH, Direction.NORTH_EAST, Direction.EAST, Direction.SOUTH_EAST, Direction.SOUTH, Direction.SOUTH_WEST, Direction.WEST, Direction.NORTH_WEST};
	public int mask = 0x03FF;
	
	public Util(MapLocation hq) {
		this.hq = hq;
	}
	
	public int convertFromBroadcastMapLocation(BroadcastMapLocation location) {
		location.x = location.x - hq.x;
		location.y = location.y - hq.y;
		return ((((location.x & mask) << 10) | (location.y & mask)) << 10) | (location.data & mask);
	}

	public BroadcastMapLocation convertToBroadcastMapLocation(int read) {

		if(read == 0) {
			return null;
		} else {
			int data = (read & mask);
			String vs1 = Integer.toBinaryString(((read >>> 10) & mask));
			if(vs1.startsWith("1") && vs1.length() == 10) { vs1 = "111111" + vs1; }
			int y = Integer.valueOf(vs1, 2).shortValue();
			vs1 = Integer.toBinaryString(((read >>> 20) & mask));
			if(vs1.startsWith("1") && vs1.length() == 10) { vs1 = "111111" + vs1; }
			int x = Integer.valueOf(vs1, 2).shortValue();
			return new BroadcastMapLocation(x + hq.x, y + hq.y, data);
		}
	}
	
	public CountInfo convertCountMessage(int read) {
		return new CountInfo((read >>> 20) & mask, (read >>> 10) & mask, (read & mask));
	}
	
	public int convertCountMessage(int currentCount, int maxCount, int buildCount) {
		return ((((currentCount & mask) << 10) | (maxCount & mask)) << 10) | (buildCount & mask);
	}
	
	static int directionToInt(Direction d) {
		switch(d) {
			case NORTH:
				return 0;
			case NORTH_EAST:
				return 1;
			case EAST:
				return 2;
			case SOUTH_EAST:
				return 3;
			case SOUTH:
				return 4;
			case SOUTH_WEST:
				return 5;
			case WEST:
				return 6;
			case NORTH_WEST:
				return 7;
			default:
				return -1;
		}
	}

}
