package team046_v05;

public enum BroadcastChannel {
		
	AEROSPACELAB(0),
	BARRACKS(1),
	BASHER(2),
	BEAVER(3),
	COMMANDER(4),
	COMPUTER(5),
	DRONE(6),
	HANDWASHSTATION(7),
	HELIPAD(8),
	LAUNCHER(9),
	MINER(10),
	MINERFACTORY(11),
	SOLDIER(12),
	SUPPLYDEPOT(13),
	TANK(14),
	TANKFACTORY(15),
	TECHNOLOGYINSTITUTE(16),
	TOWER(17),
	TRAININGFIELD(18),
	
	MININGTOP5_POSITIONS_OFFSET1(100),
	MININGTOP5_POSITIONS_OFFSET2(101),
	MININGTOP5_POSITIONS_OFFSET3(102),
	MININGTOP5_POSITIONS_OFFSET4(103),
	MININGTOP5_POSITIONS_OFFSET5(104),
	ALLIN_OFFSET(200),
	RALLY_POINT(201),
	COMMANDER_LOCATION(202),
	MINER_LOCUS(203),
	
	BUILDSEQUENCE_OFFSET(298),
	BUILDSEQUENCE_SIZE(299),
	BUILDSEQUENCE_START(300), // May go up to 350
	
	;
	
	private int channel;

	private BroadcastChannel(int channel) {
		this.channel = channel;
	}

	public int getChannel() {
		return channel;
	}
	
	public static BroadcastChannel lookupChannel(int channel) {
		for(BroadcastChannel c: BroadcastChannel.values()) {
			if(c.getChannel() == channel) {
				return c;
			}
		}
		return null;
	}
}