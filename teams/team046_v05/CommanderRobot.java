package team046_v05;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;


public class CommanderRobot extends StrategyRobot {

	public CommanderRobot(RobotSetup rs) {
		super(rs);
	}

	@Override
	public void run() throws GameActionException {
		//sendSupplies();
		
		myLocation = rc.getLocation();
		bc.broadcast(BroadcastChannel.COMMANDER_LOCATION, new BroadcastMapLocation(myLocation, 0));

		MicroSystem.myLocation = myLocation;
		MicroSystem.init();
		MicroSystem.MicroState state = MicroSystem.genericMicro();

		if(state == MicroSystem.MicroState.ATTACK) {
			if(rc.isWeaponReady()) {
				rc.attackLocation(MicroSystem.weakest.location);
			}
		} else if(state == MicroSystem.MicroState.RUN) {
			if(rc.isCoreReady()) {
				NavSystem.tryRun(MicroSystem.enemies);
			}
			
		} else if(state == MicroSystem.MicroState.MOVECLOSER) {
			if(rc.isCoreReady()) {
				NavSystem.tryMoveAvoidAttack(MicroSystem.enemies);
			}
		} else if(state == MicroSystem.MicroState.WAIT) {
			// Do nothing letting delays cool down
		} else if(state == MicroSystem.MicroState.JOB && rc.isCoreReady()) {

			// Carry on job assigmments
			
			if(job == null || job.isCompleted()) {
				job = getNextAssignment();
				NavSystem.init(job.getLocation());
			}
			
			if (rc.isCoreReady()) {
				//Direction move = Direction.NORTH;
				if(job.getJobtype() == RobotJob.RobotJobType.MOVE) {
					NavSystem.tryMoveCloser();
				} else if(job.getJobtype() == RobotJob.RobotJobType.PATROL) {
					if(myLocation.distanceSquaredTo(job.getLocation()) > 15) {
						NavSystem.tryMove(myLocation.directionTo(job.getLocation()));
					} else {
						NavSystem.tryMove(Util.directions[rand.nextInt(8)]);
					}
				} else if(job.getJobtype() == RobotJob.RobotJobType.ALLIN) {
					NavSystem.tryMoveAvoidAttack(MicroSystem.enemies);
				} else {
					NavSystem.tryMoveRandom();
				}

				if((job.getJobtype() == RobotJob.RobotJobType.MOVE || job.getJobtype() == RobotJob.RobotJobType.PATROL) && (job.getLocation() == null || job.getLocation().distanceSquaredTo(myLocation) <= 16)) {
					job.setCompleted(true);
				}
			}
		}

	}


	@Override
	public RobotJob getNextAssignment() {
		RobotJob globalJob = getGlobalJobs();
		if(globalJob != null) {
			return globalJob;
		}
		
		MapLocation[] towers = rc.senseTowerLocations();
		MapLocation patrolLocation;
		
		int fate = rand.nextInt(towers.length);
		
		if(towers.length > 1) {
			patrolLocation = towers[fate];
		} else if(towers.length == 1) {
			patrolLocation = towers[0];
		} else {
			patrolLocation = rc.senseEnemyHQLocation();
		}
		
		RobotJob job = new RobotJob(RobotJob.RobotJobType.PATROL);
		job.setLocation(patrolLocation);
		return job;
	}


}
