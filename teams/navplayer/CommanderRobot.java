package navplayer;

import battlecode.common.Clock;
import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;


public class CommanderRobot extends StrategyRobot {

	public CommanderRobot(RobotSetup rs) {
		super(rs);
	}

	@Override
	public void run() throws GameActionException {
		sendSupplies();
		
		myLocation = rc.getLocation();
		//myTeam = rc.getTeam();
		nearbyE = rc.senseNearbyRobots(mySensor, myTeam.opponent());

		if(nearbyE.length > 0) {
			int dist = myLocation.distanceSquaredTo(nearbyE[0].location);
			boolean ier = dist <= nearbyE[0].type.attackRadiusSquared;
			boolean eir = rc.canAttackLocation(nearbyE[0].location);
			if(ier) {
				Direction run = myLocation.directionTo(nearbyE[0].location).opposite();
				
				if(myLocation.add(run).distanceSquaredTo(nearbyE[0].location) > nearbyE[0].type.attackRadiusSquared) {
					if(rc.isCoreReady() && rc.canMove(run)) {
						rc.move(run);
					}
				} else if(myLocation.add(run.rotateLeft()).distanceSquaredTo(nearbyE[0].location) > nearbyE[0].type.attackRadiusSquared) {
					if(rc.isCoreReady() && rc.canMove(run.rotateLeft())) {
						rc.move(run.rotateLeft());
					}
				} else if(myLocation.add(run.rotateRight()).distanceSquaredTo(nearbyE[0].location) > nearbyE[0].type.attackRadiusSquared) {
					if(rc.isCoreReady() && rc.canMove(run.rotateRight())) {
						rc.move(run.rotateRight());
					}
				} else {
					if(rc.isWeaponReady()) {
						rc.attackLocation(nearbyE[0].location);
					}
				}
			} else if(eir) {
				if(rc.isWeaponReady()) {
					rc.attackLocation(nearbyE[0].location);
				}
			} else {
				// Nothing
			}
			
		} else {
			// Carry on job assigmments
			
			if(job == null || job.isCompleted()) {
				job = getNextAssignment();
				rc.setIndicatorString(0, "New Job: " + job + " Round: " + Clock.getRoundNum());
			} else {
				rc.setIndicatorString(0, "Working Job: " + job + " Round: " + Clock.getRoundNum());
			}
			
			if (rc.isCoreReady()) {

				if(job.getJobtype() == RobotJob.RobotJobType.MOVE) {
					NavSystem.tryMove(this, job.getLocation());
				} else if(job.getJobtype() == RobotJob.RobotJobType.PATROL) {
					if(myLocation.distanceSquaredTo(job.getLocation()) > 15) {
						NavSystem.tryMove(this, job.getLocation());
					} else {
						NavSystem.tryMoveRandom(this);
					}
				} else {
					NavSystem.tryMoveRandom(this);
				}

				if((job.getJobtype() == RobotJob.RobotJobType.MOVE || job.getJobtype() == RobotJob.RobotJobType.PATROL) && (job.getLocation() == null || job.getLocation().distanceSquaredTo(myLocation) <= 16)) {
					job.setCompleted(true);
				}
			}
		}

	}


	@Override
	public RobotJob getNextAssignment() {
		RobotJob globalJob = getGlobalJobs();
		if(globalJob != null) {
			return globalJob;
		}
		
		MapLocation[] towers = rc.senseTowerLocations();
		MapLocation patrolLocation;
		
		int fate = rand.nextInt(towers.length);
		
		if(towers.length > 1) {
			patrolLocation = towers[fate];
		} else if(towers.length == 1) {
			patrolLocation = towers[0];
		} else {
			patrolLocation = rc.senseEnemyHQLocation();
		}
		
		RobotJob job = new RobotJob(RobotJob.RobotJobType.PATROL);
		job.setLocation(patrolLocation);
		return job;
	}


}
