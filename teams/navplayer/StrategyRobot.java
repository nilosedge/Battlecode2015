package navplayer;

import battlecode.common.GameActionException;


public abstract class StrategyRobot extends BaseRobot {

	public abstract void run() throws GameActionException;

	public StrategyRobot(RobotSetup rs) {
		super(rs);
	}
	
	//public abstract void updateCounts();
	public abstract RobotJob getNextAssignment();
	//public abstract void completeJob(RobotJob job);
	//public abstract ArrayList<RobotTypeInfo> getBuildList();
	
	public RobotJob getGlobalJobs() {

		int allIn = bc.readBroadcast(BroadcastChannel.ALLIN_OFFSET);

		if(allIn != 0) {
			rc.setIndicatorString(2, "Recieved ALLIN moving to HQ");
			RobotJob job = new RobotJob(RobotJob.RobotJobType.MOVE);
			job.setLocation(rc.senseEnemyHQLocation());
			return job;
		} else {
			return null;
		}
		
		// TODO check other battle points and return one of those
		
		// TODO check other HQ jobs
		// TODO Check HQ jobs queue
		
	}
}
