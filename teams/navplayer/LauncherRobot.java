package navplayer;

import battlecode.common.GameActionException;

public class LauncherRobot extends StrategyRobot {

	public LauncherRobot(RobotSetup rs) {
		super(rs);
	}

	@Override
	public void run() throws GameActionException {
		
		sendSupplies();
		
		myLocation = rc.getLocation();
		
		if (rc.isCoreReady()) {
			
			if(job == null || job.isCompleted()) {
				job = getNextAssignment();
				rc.setIndicatorString(0, "New Job: " + job);
			}
			
			if(job != null && job.getJobtype() == RobotJob.RobotJobType.MOVE) {
				NavSystem.tryMove(this, job.getLocation());
			} else if(job != null && job.getJobtype() == RobotJob.RobotJobType.PATROL) {
				if(myLocation.distanceSquaredTo(job.getLocation()) > 15) {
					NavSystem.tryMove(this, job.getLocation());
				} else {
					NavSystem.tryMoveRandom(this);
				}
			} else {
				NavSystem.tryMoveRandom(this);
			}
			rc.setIndicatorString(1, "Missiles: " + rc.getMissileCount());
			
		}

	}


	@Override
	public RobotJob getNextAssignment() {
		
		return null;
	}


}
