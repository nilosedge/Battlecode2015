package navplayer;

import battlecode.common.RobotType;

public class RobotTypeInfo {

	public RobotType robotType;
	public int percentage = 100;
	public CountInfo cinfo;
	
	public RobotTypeInfo(RobotType robotType, CountInfo cinfo, int percentage) {
		this.robotType = robotType;
		this.cinfo = cinfo;
		this.percentage = percentage;
	}
	
	public RobotTypeInfo(RobotType robotType, CountInfo cinfo) {
		this.robotType = robotType;
		this.cinfo = cinfo;
	}

	public RobotTypeInfo(RobotType robotType) {
		this.robotType = robotType;
	}

}
