package navplayer;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;

public class NavSystem {

	public static boolean bugging = false;
	
	public static void tryMove(BaseRobot robot, Direction d) throws GameActionException {
		int offsetIndex = 0;
		int[] offsets = {0,1,-1,2,-2, 3, -3};
		int dirint = Util.directionToInt(d);
		while (offsetIndex < 7 && !robot.rc.canMove(Util.directions[(dirint+offsets[offsetIndex]+8)%8])) {
			offsetIndex++;
		}
		if (offsetIndex < 7) {
			robot.rc.move(Util.directions[(dirint+offsets[offsetIndex]+8)%8]);
		}
	}

	public static void tryMove(BaseRobot robot, MapLocation location) throws GameActionException {
		tryMove(robot, robot.myLocation.directionTo(location));
	}

	public static void tryMoveRandom(BaseRobot robot) throws GameActionException {
		tryMove(robot, Util.directions[BaseRobot.rand.nextInt(8)]);
	}

	
}
