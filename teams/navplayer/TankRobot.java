package navplayer;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;

public class TankRobot extends StrategyRobot {

	public TankRobot(RobotSetup rs) {
		super(rs);
	}

	@Override
	public void run() throws GameActionException {
		
		sendSupplies();
		
		if (rc.isWeaponReady()) {
			attackSomething();
		}

		myLocation = rc.getLocation();
		
		if (rc.isCoreReady()) {
			
			job = getNextAssignment();
			rc.setIndicatorString(1, "Job: " + job);
			
			if(job.getJobtype() == RobotJob.RobotJobType.MOVE) {
				NavSystem.tryMove(this, job.getLocation());
			} else if(job.getJobtype() == RobotJob.RobotJobType.PATROL) {
				if(myLocation.distanceSquaredTo(job.getLocation()) > 15) {
					NavSystem.tryMove(this, job.getLocation());
				} else {
					NavSystem.tryMoveRandom(this);
				}
			} else {
				NavSystem.tryMoveRandom(this);
			}

		}

	}


	@Override
	public RobotJob getNextAssignment() {
		
		RobotJob globalJob = getGlobalJobs();
		if(globalJob != null) {
			return globalJob;
		}
		
		MapLocation[] towers = rc.senseTowerLocations();
		
//		TreeMap<Integer, MapLocation> map = new TreeMap<Integer, MapLocation>();
//		
//		for(MapLocation m: towers) {
//			map.put(myLocation.distanceSquaredTo(m), m);
//		}
//		
//		RobotJob job = new RobotJob(RobotJobType.PATROL);
//		job.setLocation(map.firstEntry().getValue());
//		return job;
		
		
		int fate = rand.nextInt(towers.length);
		
		RobotJob job = new RobotJob(RobotJob.RobotJobType.PATROL);
		job.setLocation(towers[fate]);
		return job;

	}


}
