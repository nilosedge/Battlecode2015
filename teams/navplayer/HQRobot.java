package navplayer;

import java.util.ArrayList;

import battlecode.common.Clock;
import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;

public class HQRobot extends SpawnBuildingRobot {

	protected ArrayList<MapLocation> minerList = new ArrayList<MapLocation>();
	
	protected CountInfo AEROSPACELAB = new CountInfo(0, 1, 0);
	protected CountInfo BARRACKS = new CountInfo(0, 1, 0);
	protected CountInfo HANDWASHSTATION = new CountInfo(0, 10, 0);
	protected CountInfo HELIPAD = new CountInfo(0, 5, 0);
	protected CountInfo MINERFACTORY = new CountInfo(0, 1, 0);
	protected CountInfo SUPPLYDEPOT = new CountInfo(0, 100, 0);
	protected CountInfo TANKFACTORY = new CountInfo(0, 4, 0);
	protected CountInfo TECHNOLOGYINSTITUTE = new CountInfo(0, 1, 0);
	protected CountInfo TRAININGFIELD = new CountInfo(0, 1, 0);
	protected CountInfo TOWER = new CountInfo(0, 0, 0);
	
	protected CountInfo BEAVER = new CountInfo(0, 3, 0);
	protected CountInfo COMPUTER = new CountInfo(0, 1, 0);
	protected CountInfo COMMANDER = new CountInfo(0, 1, 0);
	protected CountInfo MINER = new CountInfo(0, 25, 0);
	protected CountInfo DRONE = new CountInfo(0, 25, 0);
	protected CountInfo TANK = new CountInfo(0, 60, 0);
	protected CountInfo BASHER = new CountInfo(0, 0, 0);
	protected CountInfo SOLDIER = new CountInfo(0, 0, 0);
	protected CountInfo LAUNCHER = new CountInfo(0, 60, 0);

	
	public HQRobot(RobotSetup rs, RobotTypeInfo buildType) {
		super(rs, buildType);
		myLocation = rc.senseHQLocation();
		
		bc.broadcastCount(RobotType.BEAVER, BEAVER);
		
		BroadcastChannel[] building = {
				BroadcastChannel.MINERFACTORY,
				BroadcastChannel.BARRACKS,
				BroadcastChannel.TANKFACTORY,
				BroadcastChannel.SUPPLYDEPOT,
				BroadcastChannel.TANKFACTORY,
				BroadcastChannel.SUPPLYDEPOT,
			};
		
		bc.setBuildSequence(building);

	}
	
	






	public void run() throws GameActionException {

		sendSupplies();
		
		if (rc.isWeaponReady()) {
			attackSomething();
		}

		updateCounts();
		if(Clock.getRoundNum() % 110 == 0 && Clock.getRoundNum() > 0) {
			setMiningPoints();
		}

		//RobotJob[] jobs = strat.getGlobalJobs();
		
		if (rc.isCoreReady()) {
			RobotJob job = getNextAssignment();

			if(job != null && job.getJobtype() == RobotJob.RobotJobType.SPAWN &&
				((BEAVER.currentCount < 1 && (rc.getTeamOre() >= job.getRobottype().oreCost)) || 
				(BEAVER.currentCount < BEAVER.maxCount && rc.getTeamOre() >= 1000))
			) {
				trySpawn(Util.directions[rand.nextInt(8)], job.getRobottype());
			}
			
			rc.setIndicatorString(0, "Total Ore: " + rc.getTeamOre());
		}

		if(Clock.getRoundNum() == 0) {
			System.out.println("rc.getLocation: " + rc.getLocation());
		}

		// Large Map 1600
		// Medium Map 1700
		// Small Map ??
//		if(Util.Channel.DRONE_COUNTS.getInfo().currentCount > 30) {
//			bc.sendAllInBroadCast();
//		} else {
//			bc.clearAllInBroadCast();
//		}
		if(Clock.getRoundNum() >= 1500) {
			bc.sendAllInBroadCast();
		}

	}



	public void updateCounts() {
		super.updateCounts();
		RobotInfo[] myRobots = rc.senseNearbyRobots(999999, myTeam);

		AEROSPACELAB.currentCount = 0;
		BARRACKS.currentCount = 0;
		HANDWASHSTATION.currentCount = 0;
		HELIPAD.currentCount = 0;
		MINERFACTORY.currentCount = 0;
		SUPPLYDEPOT.currentCount = 0;
		TANKFACTORY.currentCount = 0;
		TECHNOLOGYINSTITUTE.currentCount = 0;
		TRAININGFIELD.currentCount = 0;
		TOWER.currentCount = 0;
		BEAVER.currentCount = 0;
		COMPUTER.currentCount = 0;
		COMMANDER.currentCount = 0;
		MINER.currentCount = 0;
		DRONE.currentCount = 0;
		TANK.currentCount = 0;
		BASHER.currentCount = 0;
		SOLDIER.currentCount = 0;
		LAUNCHER.currentCount = 0;

		for (RobotInfo r : myRobots) {
			if(r.type == RobotType.AEROSPACELAB) { AEROSPACELAB.currentCount++; }
			else if(r.type == RobotType.BARRACKS) { BARRACKS.currentCount++; }
			else if(r.type == RobotType.HANDWASHSTATION) { HANDWASHSTATION.currentCount++; }
			else if(r.type == RobotType.HELIPAD) { HELIPAD.currentCount++; }
			else if(r.type == RobotType.MINERFACTORY) { MINERFACTORY.currentCount++; }
			else if(r.type == RobotType.SUPPLYDEPOT) { SUPPLYDEPOT.currentCount++; }
			else if(r.type == RobotType.TANKFACTORY) { TANKFACTORY.currentCount++; }
			else if(r.type == RobotType.TECHNOLOGYINSTITUTE) { TECHNOLOGYINSTITUTE.currentCount++; }
			else if(r.type == RobotType.TRAININGFIELD) { TECHNOLOGYINSTITUTE.currentCount++; }
			else if(r.type == RobotType.TOWER) { TOWER.currentCount++; }
			else if(r.type == RobotType.BEAVER) { BEAVER.currentCount++; }
			else if(r.type == RobotType.COMPUTER) { COMPUTER.currentCount++; }
			else if(r.type == RobotType.COMMANDER) { COMMANDER.currentCount++; }
			else if(r.type == RobotType.DRONE) { DRONE.currentCount++; }
			else if(r.type == RobotType.TANK) { TANK.currentCount++; }
			else if(r.type == RobotType.BASHER) { BASHER.currentCount++; }
			else if(r.type == RobotType.SOLDIER) { SOLDIER.currentCount++; }
			else if(r.type == RobotType.LAUNCHER) { LAUNCHER.currentCount++; }
			else if(r.type == RobotType.MINER) { MINER.currentCount++; minerList.add(r.location); }
		}

		bc.broadcastCount(RobotType.AEROSPACELAB, AEROSPACELAB);
		bc.broadcastCount(RobotType.BARRACKS, BARRACKS);
		bc.broadcastCount(RobotType.HANDWASHSTATION, HANDWASHSTATION);
		bc.broadcastCount(RobotType.HELIPAD, HELIPAD);
		bc.broadcastCount(RobotType.MINERFACTORY, MINERFACTORY);
		bc.broadcastCount(RobotType.SUPPLYDEPOT, SUPPLYDEPOT);
		bc.broadcastCount(RobotType.TANKFACTORY, TANKFACTORY);
		bc.broadcastCount(RobotType.TECHNOLOGYINSTITUTE, TECHNOLOGYINSTITUTE);
		bc.broadcastCount(RobotType.TRAININGFIELD, TRAININGFIELD);
		bc.broadcastCount(RobotType.TOWER, TOWER);
		bc.broadcastCount(RobotType.BEAVER, BEAVER);
		bc.broadcastCount(RobotType.COMPUTER, COMPUTER);
		bc.broadcastCount(RobotType.COMMANDER, COMMANDER);
		bc.broadcastCount(RobotType.MINER, MINER);
		bc.broadcastCount(RobotType.DRONE, DRONE);
		bc.broadcastCount(RobotType.TANK, TANK);
		bc.broadcastCount(RobotType.BASHER, BASHER);
		bc.broadcastCount(RobotType.SOLDIER, SOLDIER);
		bc.broadcastCount(RobotType.LAUNCHER, LAUNCHER);
	}

	public void setMiningPoints() {
		setMiningPoints(15);
	}
	
	public void setMiningPoints(int distance) {
		Util u = new Util(rc.senseHQLocation());
		
		if(minerList.size() < 5) {

			Direction dir = myLocation.directionTo(rc.senseEnemyHQLocation());
			
			int dirint = Util.directionToInt(dir);
			MapLocation ml = rc.getLocation();
			
			MapLocation l = ml.add(Util.directions[(dirint+6)%8], distance);
			MiningLocation m1 = new MiningLocation(l, (int)rc.senseOre(l));
			bc.broadcast(BroadcastChannel.MININGTOP5_POSITIONS_OFFSET1, u.convertFromMapLocation(m1));
			
			l = ml.add(Util.directions[(dirint+7)%8], distance);
			MiningLocation m2 = new MiningLocation(l, (int)rc.senseOre(l));
			bc.broadcast(BroadcastChannel.MININGTOP5_POSITIONS_OFFSET2, u.convertFromMapLocation(m2));
			
			l = ml.add(Util.directions[(dirint+8)%8], distance);
			MiningLocation m3 = new MiningLocation(l, (int)rc.senseOre(l));
			bc.broadcast(BroadcastChannel.MININGTOP5_POSITIONS_OFFSET3, u.convertFromMapLocation(m3));
			
			l = ml.add(Util.directions[(dirint+9)%8], distance);
			MiningLocation m4 = new MiningLocation(l, (int)rc.senseOre(l));
			bc.broadcast(BroadcastChannel.MININGTOP5_POSITIONS_OFFSET4, u.convertFromMapLocation(m4));
			
			l = ml.add(Util.directions[(dirint+10)%8], distance);
			MiningLocation m5 = new MiningLocation(l, (int)rc.senseOre(l));
			bc.broadcast(BroadcastChannel.MININGTOP5_POSITIONS_OFFSET5, u.convertFromMapLocation(m5));
			
		} else {

			MapLocation m = minerList.remove(rand.nextInt(minerList.size()));
			MiningLocation ml = new MiningLocation(m, (int)rc.senseOre(m));
			bc.broadcast(BroadcastChannel.MININGTOP5_POSITIONS_OFFSET1, u.convertFromMapLocation(ml));
			
			m = minerList.remove(rand.nextInt(minerList.size()));
			ml = new MiningLocation(m, (int)rc.senseOre(m));
			bc.broadcast(BroadcastChannel.MININGTOP5_POSITIONS_OFFSET2, u.convertFromMapLocation(ml));
			
			m = minerList.remove(rand.nextInt(minerList.size()));
			ml = new MiningLocation(m, (int)rc.senseOre(m));
			bc.broadcast(BroadcastChannel.MININGTOP5_POSITIONS_OFFSET3, u.convertFromMapLocation(ml));
		
			m = minerList.remove(rand.nextInt(minerList.size()));
			ml = new MiningLocation(m, (int)rc.senseOre(m));
			bc.broadcast(BroadcastChannel.MININGTOP5_POSITIONS_OFFSET4, u.convertFromMapLocation(ml));
			
			m = minerList.remove(rand.nextInt(minerList.size()));
			ml = new MiningLocation(m, (int)rc.senseOre(m));
			bc.broadcast(BroadcastChannel.MININGTOP5_POSITIONS_OFFSET5, u.convertFromMapLocation(ml));
				
		}
	}

}
