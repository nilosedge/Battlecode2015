package navplayer;

import java.util.TreeMap;

import battlecode.common.Clock;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotInfo;

public class MinerRobot extends StrategyRobot {
	
	protected int myOraCount = 0;
	protected int mineLowAmount = 0;
	
	public MinerRobot(RobotSetup rs, int low) {
		super(rs);
		this.mineLowAmount = low;
	}

	@Override
	public void run() throws GameActionException {

		sendSupplies();
		
		if (rc.isWeaponReady()) {
			attackSomething();
		}
		
		myOraCount = (int)rc.senseOre(rc.getLocation());
		myLocation = rc.getLocation();
		
		if (rc.isCoreReady()) {
			
			if(job == null || job.isCompleted()) {
				job = getNextAssignment();
				rc.setIndicatorString(0, "New Job: " + job + " Round: " + Clock.getRoundNum());
			}

			if(job.getJobtype() == RobotJob.RobotJobType.MOVE) {
				if(job.getLocation() != null) {
					NavSystem.tryMove(this, job.getLocation());
				} else {
					NavSystem.tryMoveRandom(this);
				}
			} else if(job.getJobtype() == RobotJob.RobotJobType.MINE) {
				rc.mine();
			}
			completeJob(job);
		}
	}
	
	
	@Override
	public RobotJob getNextAssignment() {
		
		if(myOraCount >= mineLowAmount) {
			RobotJob job = new RobotJob(RobotJob.RobotJobType.MINE);
			return job;
		}

		TreeMap<Double, TreeMap<Integer, MapLocation>> spots = findLocalMining();
		if(spots.size() > 0) {
			RobotJob job = new RobotJob(RobotJob.RobotJobType.MOVE);
			job.setLocation(spots.lastEntry().getValue().firstEntry().getValue());
			return job;
		}
		
		MiningLocation[] locs = findNewMiningLocatoin();
		MiningLocation m = locs[rand.nextInt(5)];
		if(m == null) {
			for(MiningLocation ml: locs) {
				if(ml != null) {
					m = ml;
					break;
				}
			}
		}
		
		if(m != null) {
			RobotJob job = new RobotJob(RobotJob.RobotJobType.MOVE);
			job.setLocation(m.getMapLocation());
			return job;
		}

		RobotJob job = new RobotJob(RobotJob.RobotJobType.MOVE);
		return job;
	}

	public void completeJob(RobotJob job) {
		if(job.getJobtype() == RobotJob.RobotJobType.MINE && rc.senseOre(myLocation) < mineLowAmount) {
			job.setCompleted(true);
		}
		if(job.getJobtype() == RobotJob.RobotJobType.MOVE &&
			(job.getLocation() == null || job.getLocation().distanceSquaredTo(myLocation) <= 9)) {
			job.setCompleted(true);
		}
	}

	public TreeMap<Double, TreeMap<Integer, MapLocation>> findLocalMining() {
		TreeMap<Double, TreeMap<Integer, MapLocation>> spots = new TreeMap<Double, TreeMap<Integer, MapLocation>>();
		
		MapLocation[] locals = MapLocation.getAllMapLocationsWithinRadiusSq(myLocation, 9);
		for(MapLocation m: locals) {
			double amount = rc.senseOre(m);
			RobotInfo t = null;
			try {
				t = rc.senseRobotAtLocation(m);
			} catch (GameActionException e) { e.printStackTrace(); }
			if(amount > mineLowAmount && t == null) {
				TreeMap<Integer, MapLocation> tree = spots.get(amount);
				if(tree == null) {
					tree = new TreeMap<Integer, MapLocation>();
					spots.put(amount, tree);
				}
				int distance = myLocation.distanceSquaredTo(m);
				tree.put(distance, m);
			}
		}
		return spots;
	}
	
	public MiningLocation[] findNewMiningLocatoin() {
		int read = 0;
		Util u = new Util(rc.senseHQLocation());
		MiningLocation[] locs = new MiningLocation[5];

		read = bc.readBroadcast(BroadcastChannel.MININGTOP5_POSITIONS_OFFSET1);
		locs[0] = u.convertToMiningLocation(read);
		read = bc.readBroadcast(BroadcastChannel.MININGTOP5_POSITIONS_OFFSET2);
		locs[1] = u.convertToMiningLocation(read);
		read = bc.readBroadcast(BroadcastChannel.MININGTOP5_POSITIONS_OFFSET3);
		locs[2] = u.convertToMiningLocation(read);
		read = bc.readBroadcast(BroadcastChannel.MININGTOP5_POSITIONS_OFFSET4);
		locs[3] = u.convertToMiningLocation(read);
		read = bc.readBroadcast(BroadcastChannel.MININGTOP5_POSITIONS_OFFSET5);
		locs[4] = u.convertToMiningLocation(read);

		return locs;
	}

}
