package navplayer;

import battlecode.common.GameActionException;

public class TowerRobot extends BaseRobot {

	public TowerRobot(RobotSetup rs) {
		super(rs);
	}

	@Override
	public void run() throws GameActionException {

		sendSupplies();
		
		if (rc.isWeaponReady()) {
			attackSomething();
		}
	}

}
