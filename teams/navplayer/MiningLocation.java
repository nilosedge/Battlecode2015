package navplayer;

import battlecode.common.MapLocation;

public class MiningLocation {
	public int x;
	public int y;
	public int amount;
	
	public MiningLocation(MapLocation location, int amount) {
		this.x = location.x;
		this.y = location.y;
		this.amount = amount;
	}

	public MiningLocation(int x, int y, int amount) {
		this.x = x;
		this.y = y;
		this.amount = amount;
	}

	public MapLocation getMapLocation() {
		return new MapLocation(x, y);
	}
	
	public String toString() {
		return "X: " + x + " Y: " + y + " A: " + amount;
	}
}
