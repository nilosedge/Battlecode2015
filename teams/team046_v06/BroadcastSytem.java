package team046_v06;

import battlecode.common.GameActionException;
import battlecode.common.RobotController;
import battlecode.common.RobotType;

public class BroadcastSytem {

	private RobotController rc;
	private Util u;
	
	public BroadcastSytem(RobotController rc) {
		this.rc = rc;
		u = new Util(rc.senseHQLocation());
	}

	public void broadcastCount(RobotType robotType, CountInfo cinfo) {
		broadcast(BroadcastChannel.valueOf(robotType.toString()).getChannel(), u.convertCountMessage(cinfo.currentCount, cinfo.maxCount, cinfo.buildCount));
	}

	public void broadcast(BroadcastChannel chan, int data) {
		broadcast(chan.getChannel(), data);
	}
	
	public void broadcast(int index, int data) {
		try {
			rc.broadcast(index, data);
		} catch (GameActionException e) {
			e.printStackTrace();
		}
	}
	
	public CountInfo readBroadcastCount(RobotType robotType) {
		return u.convertCountMessage(readBroadcast(BroadcastChannel.valueOf(robotType.toString()).getChannel()));
	}
	
	public int readBroadcast(BroadcastChannel chan) {
		return readBroadcast(chan.getChannel());
	}
	
	public BroadcastMapLocation readBroadcastMapLocation(BroadcastChannel chan) {
		return u.convertToBroadcastMapLocation(readBroadcast(chan.getChannel()));
	}
	
	public int readBroadcast(int index) {
		try {
			return rc.readBroadcast(index);
		} catch (GameActionException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public void sendAllInBroadCast() {
		rc.setIndicatorString(1, "Sending ALLIN Message");
		broadcast(BroadcastChannel.ALLIN_OFFSET, 1);
	}

	public void clearAllInBroadCast() {
		rc.setIndicatorString(1, "Clearing ALLIN Message");
		broadcast(BroadcastChannel.ALLIN_OFFSET, 0);
	}

	public RobotType getNextBuildSequence() {
		int bs_offset = readBroadcast(BroadcastChannel.BUILDSEQUENCE_OFFSET);
		int read = readBroadcast(BroadcastChannel.BUILDSEQUENCE_START.getChannel() + bs_offset);
		return RobotType.valueOf(BroadcastChannel.lookupChannel(read).toString());
	}
	
	public void updateBuildSequence() {
		int bs_offset = readBroadcast(BroadcastChannel.BUILDSEQUENCE_OFFSET);
		int bs_size = readBroadcast(BroadcastChannel.BUILDSEQUENCE_SIZE);
		bs_offset++;
		if(bs_offset >= bs_size) {
			bs_offset = 0;
		}
		broadcast(BroadcastChannel.BUILDSEQUENCE_OFFSET, bs_offset);
	}

	public void setBuildSequence(BroadcastChannel[] building) {
		broadcast(BroadcastChannel.BUILDSEQUENCE_SIZE, building.length);
		int offset = 0;
		for(BroadcastChannel c: building) {
			broadcast(BroadcastChannel.BUILDSEQUENCE_START.getChannel() + offset, c.getChannel());
			offset++;
		}
	}

	public void broadcast(BroadcastChannel channel, BroadcastMapLocation bml) {
		broadcast(channel, u.convertFromBroadcastMapLocation(bml));
	}

	public void setRallyPointOneQuarter(BroadcastMapLocation bml) {
		broadcast(BroadcastChannel.RALLY_POINT, u.convertFromBroadcastMapLocation(bml));
	}

}
