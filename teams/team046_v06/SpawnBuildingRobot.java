package team046_v06;

import battlecode.common.GameActionException;

public class SpawnBuildingRobot extends StrategyRobot {

	private RobotTypeInfo buildType;

	public SpawnBuildingRobot(RobotSetup rs, RobotTypeInfo robotTypeInfo) {
		super(rs);
		buildType = robotTypeInfo;
	}

	@Override
	public void run() throws GameActionException {

		if(teamOre > 0) {
			sendSupplies();
		}
		
		updateCounts();
		
		if (rc.isCoreReady()) {
			RobotJob job = getNextAssignment();
			if(job != null && job.getJobtype() == RobotJob.RobotJobType.SPAWN && teamOre >= job.getRobottype().oreCost) {
				rc.setIndicatorString(1, "Spawning: " + job.getRobottype());
				trySpawn(Util.directions[rand.nextInt(8)], job.getRobottype());
			} else {
				rc.setIndicatorString(2, "Low resources: " + rc.getTeamOre() + " Job: " + job);
			}
		}
	}

	protected void updateCounts() {
		buildType.cinfo = bc.readBroadcastCount(buildType.robotType);
		teamOre = rc.getTeamOre();
	}

	@Override
	public RobotJob getNextAssignment() {
		
		if(buildType.cinfo.currentCount < buildType.cinfo.maxCount) {
			RobotJob job = new RobotJob(RobotJob.RobotJobType.SPAWN);
			job.setRobottype(buildType.robotType);
			return job;
		}
		return null;
	}

}
