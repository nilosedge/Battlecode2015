package team046_v06;

import java.util.ArrayList;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;

public class NavSystem {

	public static boolean bugging = false;
	public static RobotSetup setup;
	public static MapLocation dest;
	
	public static boolean tryMove(Direction d) throws GameActionException {
		int offsetIndex = 0;
		int[] offsets = {0,1,-1,2,-2, 3, -3};
		int dirint = Util.directionToInt(d);
		while (offsetIndex < 7 && !setup.robot.rc.canMove(Util.directions[(dirint+offsets[offsetIndex]+8)%8])) {
			offsetIndex++;
		}
		if (offsetIndex < 7) {
			setup.robot.rc.move(Util.directions[(dirint+offsets[offsetIndex]+8)%8]);
			return true;
		}
		return false;
	}
	
	public static void tryMove(MapLocation location) throws GameActionException {
		tryMove(setup.robot.myLocation.directionTo(location));
	}
	public static void tryMoveRandom() throws GameActionException {
		tryMove(Util.directions[BaseRobot.rand.nextInt(8)]);
	}

	
	public static void tryMoveAvoidAttack(Direction d, boolean[] safe_dirs) throws GameActionException {
		int offsetIndex = 0;
		int[] offsets = {0,1,-1,2,-2, 3, -3};
		int dirint = Util.directionToInt(d);
		while (
				offsetIndex < 7 &&
				(!setup.robot.rc.canMove(Util.directions[(dirint+offsets[offsetIndex]+8)%8]) ||
				!safe_dirs[(dirint+offsets[offsetIndex]+8)%8])
				) {
			// Try different direction
			offsetIndex++;
		}
		if (offsetIndex < 7) {
			setup.robot.rc.move(Util.directions[(dirint+offsets[offsetIndex]+8)%8]);
		}
	}
	
	public static void tryMoveAvoidAttack(ArrayList<RobotInfo> enemies) throws GameActionException {
		boolean[] safe_dirs = getSafeMoveToLocations(enemies, setup.robot.myLocation);
		tryMoveAvoidAttack(setup.robot.myLocation.directionTo(dest), safe_dirs);
	}

	public static void tryMoveAvoidAttackTowers() throws GameActionException {
		boolean[] safe_dirs = getSafeMoveToLocations(setup.robot.myLocation);
		tryMoveAvoidAttack(setup.robot.myLocation.directionTo(dest), safe_dirs);
	}

	public static void init(MapLocation location) {
		dest = location;
	}
	
	public static boolean[] getSafeMoveToLocations(ArrayList<RobotInfo> enemies, MapLocation location) {
		boolean[] ret = new boolean[8];
		MapLocation[] locs = new MapLocation[8];
		for(int i = 0; i < 8; i++) {
			ret[i] = true;
			locs[i] = location.add(Util.directions[i]);
		}
		
		for(RobotInfo r: enemies) {
			for(int i = 0; i < 8; i++) {
				if(ret[i]) ret[i] = !canAttackLocation(r.location, r.type, locs[i]);
			}
		}
		
		for(MapLocation l: setup.rc.senseEnemyTowerLocations()) {
			for(int i = 0; i < 8; i++) {
				if(ret[i]) ret[i] = !canAttackLocation(l, RobotType.TOWER, locs[i]);
			}
		}
		
		MapLocation l = setup.robot.enemyLoc;
		
		for(int i = 0; i < 8; i++) {
			if(ret[i]) ret[i] = !canAttackLocation(l, RobotType.HQ, locs[i]);
		}

		return ret;
	}
	
	public static boolean[] getSafeMoveToLocations(MapLocation location) {
		boolean[] ret = new boolean[8];
		MapLocation[] locs = new MapLocation[8];
		for(int i = 0; i < 8; i++) {
			ret[i] = true;
			locs[i] = location.add(Util.directions[i]);
		}
		
		for(MapLocation l: setup.rc.senseEnemyTowerLocations()) {
			for(int i = 0; i < 8; i++) {
				if(ret[i]) ret[i] = !canAttackLocation(l, RobotType.TOWER, locs[i]);
			}
		}
		
		MapLocation l = setup.robot.enemyLoc;
		
		for(int i = 0; i < 8; i++) {
			if(ret[i]) ret[i] = !canAttackLocation(l, RobotType.HQ, locs[i]);
		}

		return ret;
	}

	public static void tryRun(ArrayList<RobotInfo> enemies) throws GameActionException {
		boolean[] dirs = getSafeMoveToLocations(enemies, setup.robot.myLocation);
		for(int i = 0; i < 8; i++) {
			if(dirs[i]) {
				if(tryMove(Direction.values()[i])) {
					return;
				}
			}
		}
	}
	
	public static boolean canAttackLocation(MapLocation attackingLocation, RobotType attackingType, MapLocation attackedLocation) {
		return attackingLocation.distanceSquaredTo(attackedLocation) <= attackingType.attackRadiusSquared;
	}
	
	public static void tryMoveCloser() throws GameActionException {
		tryMove(dest);
	}

}
