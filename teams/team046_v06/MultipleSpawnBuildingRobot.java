package team046_v06;

import java.util.ArrayList;

import battlecode.common.GameActionException;

public class MultipleSpawnBuildingRobot extends StrategyRobot {
	
	private ArrayList<RobotTypeInfo> buildTypes = new ArrayList<RobotTypeInfo>();
	
	public MultipleSpawnBuildingRobot(RobotSetup rs, ArrayList<RobotTypeInfo> buildTypes) {
		super(rs);
		this.buildTypes = buildTypes;
	}

	@Override
	public void run() throws GameActionException {

		sendSupplies();

		updateCounts();

		if (rc.isCoreReady()) {

			RobotJob job = getNextAssignment();
			if(job != null && job.getJobtype() == RobotJob.RobotJobType.SPAWN && teamOre >= job.getRobottype().oreCost) {
				rc.setIndicatorString(1, "Spawning: " + job.getRobottype());
				trySpawn(Util.directions[rand.nextInt(8)], job.getRobottype());
			} else {
				rc.setIndicatorString(2, "Low resources: " + rc.getTeamOre() + " Job: " + job);
			}
		}
	}

	protected void updateCounts() {
		for(RobotTypeInfo type: buildTypes) {
			type.cinfo = bc.readBroadcastCount(type.robotType);
		}
		teamOre = rc.getTeamOre();
	}

	@Override
	public RobotJob getNextAssignment() {

		int fate = rand.nextInt(10000);
		for(RobotTypeInfo type: buildTypes) {
			if(type.cinfo.currentCount < type.cinfo.maxCount && fate <= (type.percentage * 100)) {
				RobotJob job = new RobotJob(RobotJob.RobotJobType.SPAWN);
				job.setRobottype(type.robotType);
				return job;
			}
		}

		return null;
	}

}
