package team046_v06;

import battlecode.common.GameActionException;

public class LauncherRobot extends StrategyRobot {

	public LauncherRobot(RobotSetup rs) {
		super(rs);
	}

	@Override
	public void run() throws GameActionException {
		
		sendSupplies();
		
		myLocation = rc.getLocation();
		
		if (rc.isCoreReady()) {
			
			if(job == null || job.isCompleted()) {
				job = getNextAssignment();
				rc.setIndicatorString(0, "New Job: " + job);
			}
			
			if(job != null && job.getJobtype() == RobotJob.RobotJobType.MOVE) {
				NavSystem.tryMove(myLocation.directionTo(job.getLocation()));
			} else if(job != null && job.getJobtype() == RobotJob.RobotJobType.PATROL) {
				if(myLocation.distanceSquaredTo(job.getLocation()) > 15) {
					NavSystem.tryMove(myLocation.directionTo(job.getLocation()));
				} else {
					NavSystem.tryMove(Util.directions[rand.nextInt(8)]);
				}
			} else {
				NavSystem.tryMove(Util.directions[rand.nextInt(8)]);
			}
			rc.setIndicatorString(1, "Missiles: " + rc.getMissileCount());
			
		}

	}


	@Override
	public RobotJob getNextAssignment() {
		
		return null;
	}


}
