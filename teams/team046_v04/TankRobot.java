package team046_v04;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;

public class TankRobot extends StrategyRobot {

	public TankRobot(RobotSetup rs) {
		super(rs);
	}

	@Override
	public void run() throws GameActionException {
		
		sendSupplies();
		
		if (rc.isWeaponReady()) {
			attackSomething();
		}

		myLocation = rc.getLocation();
		
		if (rc.isCoreReady()) {
			
			job = getNextAssignment();
			rc.setIndicatorString(1, "Job: " + job);
			
			if(job.getJobtype() == RobotJob.RobotJobType.MOVE) {
				tryMove(myLocation.directionTo(job.getLocation()));
			} else if(job.getJobtype() == RobotJob.RobotJobType.PATROL) {
				if(myLocation.distanceSquaredTo(job.getLocation()) > 15) {
					tryMove(myLocation.directionTo(job.getLocation()));
				} else {
					tryMove(Util.directions[rand.nextInt(8)]);
				}
			} else {
				tryMove(Util.directions[rand.nextInt(8)]);
			}

		}

	}


	@Override
	public RobotJob getNextAssignment() {
		
		RobotJob globalJob = getGlobalJobs();
		if(globalJob != null) {
			return globalJob;
		}
		
		MapLocation[] towers = rc.senseTowerLocations();
		
//		TreeMap<Integer, MapLocation> map = new TreeMap<Integer, MapLocation>();
//		
//		for(MapLocation m: towers) {
//			map.put(myLocation.distanceSquaredTo(m), m);
//		}
//		
//		RobotJob job = new RobotJob(RobotJobType.PATROL);
//		job.setLocation(map.firstEntry().getValue());
//		return job;
		
		if(towers.length > 0) {
			int fate = rand.nextInt(towers.length);
			
			RobotJob job = new RobotJob(RobotJob.RobotJobType.PATROL);
			job.setLocation(towers[fate]);
			return job;
		} else {
			return null;
		}

	}


}
