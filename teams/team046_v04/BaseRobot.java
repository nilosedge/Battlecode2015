package team046_v04;

import java.util.Random;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.GameConstants;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import battlecode.common.Team;

public abstract class BaseRobot {
	
	public RobotController rc;
	public int id;
	public Team myTeam;
	public Team enemyTeam;
	public int myAttack;
	protected RobotJob job;
	public MapLocation enemyLoc;
	public static Random rand;
	public RobotInfo[] myRobots;
	protected BroadcastSytem bc;
	public int mySensor;
	public MapLocation myLocation;
	protected MapLocation hqLocation;
	public double teamOre;
	public RobotInfo[] nearbyE;
	
	public BaseRobot(RobotSetup rs) { // RobotController rc, Strategy strat, BroadcastSytem bc) {
		this.rc = rs.rc;
		this.bc = rs.bc;
		id = rc.getID();
		rand = new Random(id);
		myTeam = rc.getTeam();
		enemyTeam = myTeam.opponent();
		myAttack = rc.getType().attackRadiusSquared;
		mySensor = rc.getType().sensorRadiusSquared;
		enemyLoc = rc.senseEnemyHQLocation();
		hqLocation = rc.senseHQLocation();
	}
	
	abstract public void run() throws GameActionException;
	
	public void loop() {
		while (true) {
			try {
				run();
			} catch (Exception e) {
				e.printStackTrace();
			}
			rc.yield();
		}
	}
	
	protected void attackSomething() throws GameActionException {
		RobotInfo[] enemies = rc.senseNearbyRobots(myAttack, enemyTeam);
		if (enemies.length > 0) {
			rc.attackLocation(enemies[0].location);
		}
	}
	
	public void tryMove(Direction d) throws GameActionException {
		int offsetIndex = 0;
		int[] offsets = {0,1,-1,2,-2, 3, -3};
		int dirint = Util.directionToInt(d);
		while (offsetIndex < 7 && !rc.canMove(Util.directions[(dirint+offsets[offsetIndex]+8)%8])) {
			offsetIndex++;
		}
		if (offsetIndex < 7) {
			rc.move(Util.directions[(dirint+offsets[offsetIndex]+8)%8]);
		}
	}
	
	public MapLocation[] getBestMiningLocations() {
		//rc.setIndicatorString(1, "THis is a test");
		//rc.senseOre(arg0)
		//rc.sen
		return null;
	}
	
	public void trySpawn(Direction d, RobotType type) throws GameActionException {
		int offsetIndex = 0;
		int[] offsets = {0,1,-1,2,-2,3,-3,4};
		int dirint = Util.directionToInt(d);
		while (offsetIndex < 8 && !rc.canSpawn(Util.directions[(dirint+offsets[offsetIndex]+8)%8], type)) {
			offsetIndex++;
		}
		if (offsetIndex < 8) {
			rc.spawn(Util.directions[(dirint+offsets[offsetIndex]+8)%8], type);
		}
	}
	
	public void sendSupplies() {
		double mylevel = rc.getSupplyLevel();
		if(mylevel < 50) return;
		RobotInfo[] myRobots = rc.senseNearbyRobots(GameConstants.SUPPLY_TRANSFER_RADIUS_SQUARED, myTeam);
		double lowest = mylevel;
		RobotInfo supplyRobot = null;
		for(RobotInfo ri: myRobots) {
			if(ri.supplyLevel < lowest) {
				supplyRobot = ri;
				lowest = ri.supplyLevel;
			}
		}
		if(supplyRobot != null) {
			try { rc.transferSupplies((int)((mylevel - supplyRobot.supplyLevel) / 2), supplyRobot.location); } catch (GameActionException e) { e.printStackTrace(); }
		}
	}
	
	// This method will attempt to build in the given direction (or as close to it as possible)
	public void tryBuild(Direction d, RobotType type) throws GameActionException {
		int offsetIndex = 0;
		int[] offsets = {0,1,-1,2,-2,3,-3,4};
		int dirint = Util.directionToInt(d);
		while (offsetIndex < 8 && !rc.canMove(Util.directions[(dirint+offsets[offsetIndex]+8)%8])) {
			offsetIndex++;
		}
		if (offsetIndex < 8) {
			rc.build(Util.directions[(dirint+offsets[offsetIndex]+8)%8], type);
		}
	}

}