package team046_v04;

import battlecode.common.MapLocation;
import battlecode.common.RobotType;


public class RobotJob {

	RobotJobType jobtype;
	RobotType robottype;
	MapLocation location;
	int amount = 0;
	boolean completed = false;
	
	public RobotJob(RobotJobType jobtype) {
		this.jobtype = jobtype;
	}
	public RobotJobType getJobtype() {
		return jobtype;
	}
	public void setJobtype(RobotJobType jobtype) {
		this.jobtype = jobtype;
	}
	public RobotType getRobottype() {
		return robottype;
	}
	public void setRobottype(RobotType robottype) {
		this.robottype = robottype;
	}
	public MapLocation getLocation() {
		return location;
	}
	public void setLocation(MapLocation location) {
		this.location = location;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public boolean isCompleted() {
		return completed;
	}
	public void setCompleted(boolean completed) {
		this.completed = completed;
	}

	public enum RobotJobType {
		BUILD,
		MINE,
		MOVE,
		SPAWN,
		PATROL
	}
	
	public String toString() {
		return "JT: " + jobtype + " RT: " + robottype + " L: " + location + " A: " + amount + " C: " + completed;
	}
}
