#!/usr/bin/ruby

require 'zlib'
require 'rexml/document'
include REXML

xml = "<matches>"

Dir.foreach("matches").each do |f|
	next if f == '.' or f == '..'

	puts "Opening: " + f
	infile = open("matches/" + f)
	gz = Zlib::GzipReader.new(infile)


	gz.each_line do |line|
		if line =~ /<map class/
			xml += "<match>\n"
			xml += line
		end
		if line =~ /<\/map>/
			xml += line
		end
		if line =~ /ser.GameStats/
			xml += line
		end
		if line =~ /ser.ExtensibleMetadata/
			xml += line
		end
		if line =~ /<ser.MatchFooter/
			xml += line
		end
		if line =~ /<\/ser.MatchFooter/
			xml += line
			xml += "</match>\n"
		end
	end

	#xmldoc = Document.new(gz)
	#puts xmldoc

end
xml += "</matches>"

xmldoc = Document.new(xml)

root = xmldoc.root
mapname = "", teamA = "", teamB = "", win = ""
stats = {}
root.elements.each("match") do |match|
	match.elements.each("map") do |m|
		mapname = m.attributes["mapName"]
	end
	match.elements.each("ser.ExtensibleMetadata") do |se|
		teamA = se.attributes["team-a"]
		teamB = se.attributes["team-b"]
	end
	match.elements.each("ser.MatchFooter") do |mf|
		win = mf.attributes["winner"]
	end

	#puts " " + mapname + " " + teamA + " " + teamB + " " + win

	if(!stats[teamA]) then stats[teamA] = {
		"A" => { :wins => 0, :loss => 0 },
		"B" => { :wins => 0, :loss => 0 },
		:total => { :wins => 0, :loss => 0 }
	} end
	if(!stats[teamA][mapname]) then stats[teamA][mapname] = { :wins => 0, :loss => 0 } end

	if(!stats[teamB]) then stats[teamB] = {
		"A" => { :wins => 0, :loss => 0 },
		"B" => { :wins => 0, :loss => 0 },
		:total => { :wins => 0, :loss => 0 }
	} end
	if(!stats[teamB][mapname]) then stats[teamB][mapname] = { :wins => 0, :loss => 0 } end


	if (win == "A") then
		stats[teamA][:total][:wins] += 1
		stats[teamA][mapname][:wins] += 1
		stats[teamA][win][:wins] += 1
		stats[teamB][:total][:loss] += 1
		stats[teamB][mapname][:loss] += 1
		stats[teamB]["B"][:loss] += 1
	end

	if (win == "B") then
		stats[teamB][:total][:wins] += 1
		stats[teamB][mapname][:wins] += 1
		stats[teamB][win][:wins] += 1
		stats[teamA][:total][:loss] += 1
		stats[teamA][mapname][:loss] += 1
		stats[teamA]["A"][:loss] += 1
	end

end

puts stats


